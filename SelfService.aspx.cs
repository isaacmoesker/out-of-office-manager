﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Web.UI.WebControls;

public partial class SelfService : System.Web.UI.Page
{
  private string ldapBase = "LDAP://DC=CLEARWATERCOUNTY,DC=CA";

  protected void Page_Load(object sender, EventArgs e)
  {
    if (!IsPostBack)
    {
      btnSubmitDepartment.Click += new EventHandler(ClickDepartment);
      btnSubmitManager.Click += new EventHandler(ClickManager);
      string sAMAccountName = System.Web.HttpContext.Current.Request.LogonUserIdentity.Name.Split('\\')[1];
      ChooseDepartment(sAMAccountName);
    }
  }

  private void ChooseDepartment(string sAMAccountName)
  {
    string department = "";
    using (DirectoryEntry directoryEntry = new DirectoryEntry(ldapBase))
    {
      // Get current user distinguishedName
      using (DirectorySearcher ds = new DirectorySearcher(directoryEntry))
      {
        ds.SearchScope = SearchScope.Subtree;
        ds.PropertiesToLoad.Add("department");
        ds.Filter = "(&(objectCategory=user)(sAMAccountName=" + sAMAccountName + "))";

        SearchResult sr = ds.FindOne();
        if (sr != null)
        {
          // create new object from search result
          DirectoryEntry entryToUpdate = sr.GetDirectoryEntry();
          department = (string)entryToUpdate.Properties["department"].Value;
          entryToUpdate.CommitChanges();
        }
      }
    }

    if (String.IsNullOrWhiteSpace(department))
    {
      lblCategory.Text = "Department";
      ddlItems.Items.Clear();

      using (DirectoryEntry directoryEntry = new DirectoryEntry(ldapBase))
      {
        // Get current user distinguishedName
        using (DirectorySearcher ds = new DirectorySearcher(directoryEntry))
        {
          ds.Filter = "(objectCategory=user)";
          ds.PropertiesToLoad.Add("department");

          using (SearchResultCollection src = ds.FindAll())
          {
            foreach (SearchResult sr in src)
            {
              DirectoryEntry de = sr.GetDirectoryEntry();
              if (de.Properties.Contains("department"))
              {
                ListItem dept = new ListItem(de.Properties["department"][0].ToString());
                if (!ddlItems.Items.Contains(dept))
                  ddlItems.Items.Add(dept);
              }
            }
          }
        }
      }


      btnSubmitDepartment.Visible = true;
      btnSubmitManager.Visible = false;
      btnSubmitDepartment.CommandName = sAMAccountName;
    }
    else ChooseManager(department, sAMAccountName);
  }

  private void ChooseManager(string department, string sAMAccountName)
  {
    lblCategory.Text = "Manager";
    ddlItems.Items.Clear();

    using (DirectoryEntry directoryEntry = new DirectoryEntry(ldapBase))
    {
      // Get current user distinguishedName
      using (DirectorySearcher ds = new DirectorySearcher(directoryEntry))
      {
        ds.SearchScope = SearchScope.Subtree;
        ds.PropertiesToLoad.Clear();
        ds.PropertiesToLoad.Add("displayName");
        ds.PropertiesToLoad.Add("distinguishedName");
        ds.ServerPageTimeLimit = TimeSpan.FromSeconds(2);
        ds.Filter = string.Format("(&(objectCategory=user)(department=" + department + "))");

        using (SearchResultCollection src = ds.FindAll())
        {
          foreach (SearchResult sr in src)
          {
            ddlItems.Items.Add(new ListItem((string)sr.Properties["displayName"][0], (string)sr.Properties["distinguishedName"][0]));
          }
        }
      }
    }

    btnSubmitDepartment.Visible = false;
    btnSubmitManager.Visible = true;
    btnSubmitManager.CommandName = sAMAccountName;
  }

  protected void ClickDepartment(object sender, EventArgs e)
  {
    LinkButton linkButton = (LinkButton)sender;
    string sAMAccountName = linkButton.CommandName;
    string department = ddlItems.SelectedValue;

    using (DirectoryEntry directoryEntry = new DirectoryEntry(ldapBase))
    {
      // Get current user distinguishedName
      using (DirectorySearcher directorySearcher = new DirectorySearcher(directoryEntry))
      {
        directorySearcher.Filter = "(sAMAccountName=" + sAMAccountName + ")";
        directorySearcher.PropertiesToLoad.Add("department");

        SearchResult result = directorySearcher.FindOne();

        if (result != null)
        {
          // create new object from search result
          using (DirectoryEntry entryToUpdate = result.GetDirectoryEntry())
          {
            entryToUpdate.Properties["department"].Value = department;
            entryToUpdate.CommitChanges();
          }
        }
      }
    }

    ChooseManager(department, sAMAccountName);
  }

  protected void ClickManager(object sender, EventArgs e)
  {
    LinkButton linkButton = (LinkButton)sender;
    string sAMAccountName = linkButton.CommandName;
    string manager = ddlItems.SelectedValue;

    using (DirectoryEntry directoryEntry = new DirectoryEntry(ldapBase))
    {
      // Get current user distinguishedName
      using (DirectorySearcher directorySearcher = new DirectorySearcher(directoryEntry))
      {
        directorySearcher.Filter = "(sAMAccountName=" + sAMAccountName + ")";
        directorySearcher.PropertiesToLoad.Add("manager");

        SearchResult result = directorySearcher.FindOne();

        if (result != null)
        {
          // create new object from search result
          using (DirectoryEntry entryToUpdate = result.GetDirectoryEntry())
          {
            entryToUpdate.Properties["manager"].Value = manager;
            entryToUpdate.CommitChanges();
          }
        }
      }
    }

    Response.Redirect("/");
  }
}