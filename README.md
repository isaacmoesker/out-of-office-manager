# Description #

Out of Office Manager is a pre-programmed mini-application that streamlines and automates Out of Office requests - from collecting event information, giving feedback, and approving calendar items, to tracking the current status of a request. Out of Office Manager is designed to save you time and effort, and to bring consistency and efficiency to tasks that you perform on a regular basis.

# Setup #

See "Documentation" directory.