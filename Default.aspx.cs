﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Security.Principal;
using System.Web;
using System.Web.UI.WebControls;
using System.Xml;

public partial class _Default : System.Web.UI.Page
{
  private SharePointUser logonUser;
  private List<SharePointUser> directReports = new List<SharePointUser>();

  protected void Page_Load(object sender, EventArgs e)
  {
    // Create logon user
    string[] logonUserIdentity = HttpContext.Current.Request.LogonUserIdentity.Name.Split('\\');
    logonUser = new SharePointUser(logonUserIdentity[1], logonUserIdentity[0]);
    logonUser.RetrieveMyItems();
    logonUser.RetrievePendingItems();
    BuildTableMy(logonUser.MyItems, "My");
    BuildTableMy(logonUser.PendingItems, "Pending");
    BuildFooterCreate(tableMy);

    // Create direct reports
    logonUser.RetrieveOrganizationStructure();

    if (String.IsNullOrEmpty(logonUser.Manager)) WriteException("Default.Page_Load", "Manager field is null or empty.");

    for (int i = 0; i < logonUser.DirectReports.Count; i++)
    {
      SharePointUser directReport = new SharePointUser(logonUser.DirectReports[i], logonUserIdentity[0]);
      directReport.RetrieveMyItems();
      BuildTableDirectReports(directReport.MyItems);
      directReports.Add(directReport);
    }

    logonUser.RetrieveActionItems();
    BuildTableAction();
  }

  private void BuildTableMy(Dictionary<object, SharePointListItem> list, string commandName)
  {
    try
    {
      if (list.Count > 0)
      {
        foreach (object id in list.Keys)
        {
          // Create table row and cells
          TableRow row = new TableRow();
          TableCell cell0 = new TableCell();
          TableCell cell1 = new TableCell();
          TableCell cell2 = new TableCell();
          TableCell cell3 = new TableCell();

          // Set control properties
          row.TableSection = TableRowSection.TableBody;
          cell3.HorizontalAlign = HorizontalAlign.Center;

          // Build update and delete actions
          LinkButton revealAUpdate = new LinkButton();
          revealAUpdate.ID = tableMy.ID + "update" + id + commandName;
          revealAUpdate.CssClass = "fa fa-pencil fa-fw fa-lg";
          revealAUpdate.CommandName = commandName;
          revealAUpdate.CommandArgument = id.ToString();
          revealAUpdate.OnClientClick = "$(this).toggleClass('fa-pencil fa-cog').addClass('fa-spin');";
          revealAUpdate.Click += new EventHandler(ClickUpdate);

          LinkButton revealADelete = new LinkButton();
          revealADelete.ID = tableMy.ID + "delete" + id + commandName;
          revealADelete.CssClass = "fa fa-trash fa-fw fa-lg";
          revealADelete.CommandName = commandName;
          revealADelete.CommandArgument = id.ToString();
          revealADelete.OnClientClick = "$(this).toggleClass('fa-trash fa-cog').addClass('fa-spin');";
          revealADelete.Click += new EventHandler(ClickDelete);

          if (commandName == "Pending")
            cell0.Text = "<span class='info label'><i class='fa fa-clock-o' aria-hidden='true'></i> "
              + commandName + "</span> " + (string)list[id].Title;
          else
            cell0.Text = list[id].Title.ToString();

          cell1.Text = list[id].EventDate.ToString("MMMM d, yyyy h:mm tt");
          cell2.Text = DoctorEndDate(list[id]).ToString("MMMM d, yyyy h:mm tt");
          cell3.Controls.Add(revealAUpdate);
          cell3.Controls.Add(revealADelete);
          row.Cells.Add(cell0);
          row.Cells.Add(cell1);
          row.Cells.Add(cell2);
          row.Cells.Add(cell3);
          tableMy.Rows.Add(row);

          tableMyHeader.Visible = true;
        }
      }
    }
    catch (Exception exception)
    {
      WriteException("Default.BuildTableMy", exception.ToString());
    }
  }

  private void BuildFooterCreate(Table table)
  {
    try
    {
      LinkButton create = new LinkButton();
      create.ID = table.ID + "FooterAdd";
      create.CssClass = "fa fa-plus fa-fw fa-lg";
      create.OnClientClick = "$(this).toggleClass('fa-plus fa-cog').addClass('fa-spin');";
      create.Click += new EventHandler(ClickCreate);

      TableCell cell = new TableCell();
      cell.ColumnSpan = 4;
      cell.HorizontalAlign = HorizontalAlign.Center;
      cell.Controls.Add(create);

      TableFooterRow footer = new TableFooterRow();
      footer.TableSection = TableRowSection.TableFooter;
      footer.Cells.Add(cell);

      table.Rows.Add(footer);
      table.Visible = true;
    }
    catch (Exception exception)
    {
      WriteException("Default.BuildFooterCreate", exception.ToString());
    }
  }

  private void BuildTableDirectReports(Dictionary<object, SharePointListItem> list)
  {
    try
    {
      if (list.Count > 0)
      {
        foreach (object id in list.Keys)
        {
          // Create table row and cells
          TableRow row = new TableRow();
          TableCell cell0 = new TableCell();
          TableCell cell1 = new TableCell();
          TableCell cell2 = new TableCell();
          TableCell cell3 = new TableCell();

          // Set control properties
          row.TableSection = TableRowSection.TableBody;
          string personLookupVal = ((Microsoft.SharePoint.Client.FieldUserValue)list[id].Person).LookupValue;

          cell0.Text = (string)list[id].Title;
          cell1.Text = list[id].EventDate.ToString("MMMM d, yyyy h:mm tt");
          cell2.Text = DoctorEndDate(list[id]).ToString("MMMM d, yyyy h:mm tt");
          cell3.Text = list[id].DecodeRecurrenceData();

          row.Cells.Add(cell0);
          row.Cells.Add(cell1);
          row.Cells.Add(cell2);
          row.Cells.Add(cell3);
          tableDirectReports.Rows.Add(row);
        }
        panelDirectReports.Visible = true;
      }
    }
    catch (Exception exception)
    {
      WriteException("Default.BuildTableDirectReports", exception.ToString());
    }
  }

  private void BuildTableAction()
  {
    try
    {
      if (logonUser.ActionItems.Count > 0)
      {
        foreach (object id in logonUser.ActionItems.Keys)
        {
          // Create table row and cells
          TableRow row = new TableRow();
          TableCell cell0 = new TableCell();
          TableCell cell1 = new TableCell();
          TableCell cell2 = new TableCell();
          TableCell cell3 = new TableCell();

          // Set control properties
          row.TableSection = TableRowSection.TableBody;
          cell3.HorizontalAlign = HorizontalAlign.Center;
          string personLookupVal = ((Microsoft.SharePoint.Client.FieldUserValue)logonUser.ActionItems[id].Person).LookupValue;

          // Build update and delete actions
          LinkButton approve = new LinkButton();
          approve.ID = tableAction.ID + "RevealApprove" + id;
          approve.CssClass = "fa fa-check fa-fw fa-lg";
          approve.OnClientClick = "$(this).toggleClass('fa-check fa-cog').addClass('fa-spin');";
          approve.CommandName = personLookupVal;
          approve.CommandArgument = id.ToString();
          approve.Click += new EventHandler(ClickApprove);

          LinkButton reject = new LinkButton();
          reject.ID = tableAction.ID + "RevealReject" + id;
          reject.CssClass = "fa fa-times fa-fw fa-lg";
          reject.OnClientClick = "$(this).toggleClass('fa-times fa-cog').addClass('fa-spin');";
          reject.CommandName = personLookupVal;
          reject.CommandArgument = id.ToString();
          reject.Click += new EventHandler(ClickReject);

          cell0.Text = (string)logonUser.ActionItems[id].Title;
          cell1.Text = logonUser.ActionItems[id].EventDate.ToString("MMMM d, yyyy h:mm tt");
          cell2.Text = DoctorEndDate(logonUser.ActionItems[id]).ToString("MMMM d, yyyy h:mm tt");
          cell3.Controls.Add(approve);
          cell3.Controls.Add(reject);
          row.Cells.Add(cell0);
          row.Cells.Add(cell1);
          row.Cells.Add(cell2);
          row.Cells.Add(cell3);
          tableAction.Rows.Add(row);
        }
        panelAction.Visible = true;
      }
    }
    catch (Exception exception)
    {
      WriteException("Default.BuildTableAction", exception.ToString());
    }
  }

  private void ClickUpdate(object sender, EventArgs e)
  {
    try
    {
      LinkButton linkButton = (LinkButton)sender;
      int id = Int32.Parse(linkButton.CommandArgument);
      string name = linkButton.CommandName;
      Dictionary<object, SharePointListItem> list;

      if (name == "My")
        list = logonUser.MyItems;
      else
      {
        list = logonUser.PendingItems;
        name = "Pending";
      }

      // Build body
      formValidation.Visible = false;
      updatePLead.Text = "<p class='lead'>Edit Item</p>";

      updateTxtTitle.Text = (string)list[id].Title.ToString().Split('-')[1].Trim();
      updateTxtDescription.Text = (string)list[id].Description;
      updateDdlDepartment.SelectedValue = (string)list[id].Department;
      updateAllDayEvent.Checked = list[id].FAllDayEvent;
      SetRecurControls(list[id], sender, e);

      // Construct Start/End times
      updateTxtStartDate.Text = list[id].EventDate.ToString("yyyy-MM-dd");
      updateTxtEndDate.Text = DoctorEndDate(list[id]).ToString("yyyy-MM-dd");

      if (!updateAllDayEvent.Checked)
      {
        updateTxtStartTime.Enabled = true;
        updateTxtEndTime.Enabled = true;
        updateTxtStartTime.Text = list[id].EventDate.ToString("hh:mm:ss");
        updateTxtEndTime.Text = DoctorEndDate(list[id]).ToString("hh:mm:ss");
      }
      else
      {
        updateTxtStartTime.Enabled = false;
        updateTxtEndTime.Enabled = false;
        updateTxtStartTime.Text = "00:00:00";
        updateTxtEndTime.Text = "23:59:00";
      }

      SetJavaScript("update");

      // Build buttons
      updateSuccess.CommandName = name;
      updateSuccess.CommandArgument = id.ToString();
      updateSuccess.Visible = true;
      updateCreateSuccess.Visible = false;
    }
    catch (Exception exception)
    {
      WriteException("Default.ClickUpdate", exception.ToString());
    }
  }

  private void ClickDelete(object sender, EventArgs e)
  {
    try
    {
      LinkButton linkButton = (LinkButton)sender;
      int id = Int32.Parse(linkButton.CommandArgument);
      string name = linkButton.CommandName;
      Dictionary<object, SharePointListItem> list;

      if (name == "My")
        list = logonUser.MyItems;
      else
      {
        list = logonUser.PendingItems;
        name = "Pending";
      }

      string[] eventDateTime = list[id].DecodeDateTime(list[id].EventDate);
      string[] endDateTime = list[id].DecodeDateTime(DoctorEndDate(list[id]));

      // Build body
      deletePLead.Text = "<p class='lead'>Are you sure you want to delete this item?</p>";
      deleteP.Text = "<p>Title: " + list[id].Title +
        "<br>Department: " + list[id].Department +
        "<br>Recur: " + list[id].DecodeRecurrenceData() +
        "<br>Start Date: " + list[id].EventDate.ToString("MMMM d, yyyy h:mm tt") +
        "<br>End Date: " + DoctorEndDate(list[id]).ToString("MMMM d, yyyy h:mm tt") +
        "<br>Description: " + list[id].Description + "</p>";

      // Build buttons
      deleteSuccess.CommandName = name;
      deleteSuccess.CommandArgument = id.ToString();
      deleteSuccess.Visible = true;
      revealApproveSuccess.Visible = false;
      revealRejectSuccess.Visible = false;
      deleteLblWhy.Visible = true;
      deleteWhy.Visible = true;

      // Open the reveal
      SetJavaScript("delete");
    }
    catch (Exception exception)
    {
      WriteException("Default.ClickDelete", exception.ToString());
    }
  }

  private void ClickCreate(object sender, EventArgs e)
  {
    try
    {
      // Build body
      formValidation.Visible = false;
      updatePLead.Text = "<p id='updatePLead' class='lead'>Create Item</p>";

      // Build buttons
      updateCreateSuccess.Visible = true;
      updateSuccess.Visible = false;

      // Open the reveal
      SetJavaScript("update");
    }
    catch (Exception exception)
    {
      WriteException("Default.ClickCreate", exception.ToString());
    }
  }

  private void ClickApprove(object sender, EventArgs e)
  {
    try
    {
      LinkButton linkButton = (LinkButton)sender;
      int id = Int32.Parse(linkButton.CommandArgument);

      // Build body
      deletePLead.Text = "<p class='lead'>Are you sure you want to approve this item?</p>";
      deleteP.Text = "<p>Title: " + logonUser.ActionItems[id].Title +
        "<br>Department: " + logonUser.ActionItems[id].Department +
        "<br>Recur: " + logonUser.ActionItems[id].DecodeRecurrenceData() +
        "<br>Start Date: " + logonUser.ActionItems[id].EventDate.ToString("MMMM d, yyyy h:mm tt") +
        "<br>End Date: " + DoctorEndDate(logonUser.ActionItems[id]).ToString("MMMM d, yyyy h:mm tt") +
        "<br>Description: " + logonUser.ActionItems[id].Description + "</p>";

      // Build buttons
      revealApproveSuccess.CommandName = linkButton.CommandName;
      revealApproveSuccess.CommandArgument = id.ToString();
      revealApproveSuccess.Visible = true;
      deleteSuccess.Visible = false;
      revealRejectSuccess.Visible = false;
      deleteLblWhy.Visible = false;
      deleteWhy.Visible = false;

      // Open the reveal
      SetJavaScript("delete");
    }
    catch (Exception exception)
    {
      WriteException("Default.ClickApprove", exception.ToString());
    }
  }

  private void ClickReject(object sender, EventArgs e)
  {
    try
    {
      LinkButton linkButton = (LinkButton)sender;
      int id = Int32.Parse(linkButton.CommandArgument);

      // Build body
      deletePLead.Text = "<p class='lead'>Are you sure you want to reject this item?</p>";
      deleteP.Text = "<p>Title: " + logonUser.ActionItems[id].Title +
        "<br>Department: " + logonUser.ActionItems[id].Department +
        "<br>Recur: " + logonUser.ActionItems[id].DecodeRecurrenceData() +
        "<br>Start Date: " + logonUser.ActionItems[id].EventDate.ToString("MMMM d, yyyy h:mm tt") +
        "<br>End Date: " + DoctorEndDate(logonUser.ActionItems[id]).ToString("MMMM d, yyyy h:mm tt") +
        "<br>Description: " + logonUser.ActionItems[id].Description + "</p>";

      // Build buttons
      revealRejectSuccess.CommandArgument = id.ToString();
      revealRejectSuccess.Visible = true;
      deleteSuccess.Visible = false;
      revealApproveSuccess.Visible = false;
      deleteLblWhy.Visible = true;
      deleteWhy.Visible = true;

      // Open the reveal
      SetJavaScript("delete");
    }
    catch (Exception exception)
    {
      WriteException("Default.ClickReject", exception.ToString());
    }
  }

  protected void ClickUpdateSuccess(object sender, EventArgs e)
  {
    try
    {
      if (ValidateUpdateForm())
      {
        LinkButton linkButton = (LinkButton)sender;
        int id = Int32.Parse(linkButton.CommandArgument);
        string name = linkButton.CommandName;
        Dictionary<object, SharePointListItem> list;

        if (name == "My")
          list = logonUser.MyItems;
        else
        {
          list = logonUser.PendingItems;
          name = "Pending";
        }

        // Set values and update SP list
        list[id].Title = logonUser.Title + " - " + updateTxtTitle.Text;
        list[id].EventDate = EncodeDateTime(updateTxtStartDate.Text, updateTxtStartTime.Text, false);
        list[id].EndDate = EncodeDateTime(updateTxtEndDate.Text, updateTxtEndTime.Text, true);
        list[id].Description = updateTxtDescription.Text;
        list[id].Department = updateDdlDepartment.SelectedValue;
        list[id].FAllDayEvent = updateAllDayEvent.Checked;
        list[id].FRecurrence = updateRecurringChb.Checked;
        list[id].RecurrenceData = WrapRecurringXML();
        list[id].EventType = GetEventType(updateRecurringChb.Checked);

        EncodeRecurrenceData(list[id]);
        list[id].EventDate = list[id].EventDate.ToUniversalTime();

        logonUser.EmailUser(logonUser.Manager,
          "<h1>Item Updated</h1>" +
          "<p>Title: " + list[id].Title +
          "<br>Department: " + list[id].Department +
          "<br>Recur: " + list[id].DecodeRecurrenceData() +
          "<br>Start Date: " + list[id].EventDate.ToString("MMMM d, yyyy h:mm tt") +
          "<br>End Date: " + DoctorEndDate(list[id]).ToString("MMMM d, yyyy h:mm tt") +
          "<br>Description: " + list[id].Description +
          "</p><p>Click <a href='http://ooom.clearwatercounty.ca/'>here</a> to approve or reject this item.</p>");

        logonUser.UpdateListItem(list[id], name);

        Response.Redirect("/", false);
      }
      else
        SetJavaScript("update");
    }
    catch (Exception exception)
    {
      WriteException("Default.ClickUpdateSuccess", exception.ToString());
    }
  }

  protected void ClickDeleteSuccess(object sender, EventArgs e)
  {
    try
    {
      LinkButton linkButton = (LinkButton)sender;
      int id = Int32.Parse(linkButton.CommandArgument);
      string commandName = linkButton.CommandName;
      Dictionary<object, SharePointListItem> list;

      if (commandName == "My")
        list = logonUser.MyItems;
      else
        list = logonUser.PendingItems;

      logonUser.EmailUser(logonUser.Manager,
        "<h1>Item Deleted</h1>" +
        "<p>Title: " + list[id].Title +
        "<br>Department: " + list[id].Department +
        "<br>Recur: " + list[id].DecodeRecurrenceData() +
        "<br>Start Date: " + list[id].EventDate.ToString("MMMM d, yyyy h:mm tt") +
        "<br>End Date: " + DoctorEndDate(list[id]).ToString("MMMM d, yyyy h:mm tt") +
        "<br>Description: " + list[id].Description +
        "<br>Reason: " + deleteWhy.Text + "</p>");

      logonUser.DeleteListItem(list[id]);

      Response.Redirect("/", false);
    }
    catch (Exception exception)
    {
      WriteException("Default.ClickDeleteSuccess", exception.ToString());
    }
  }

  protected void ClickCreateSuccess(object sender, EventArgs e)
  {
    try
    {
      if (ValidateUpdateForm())
      {
        SharePointListItem newItem = logonUser.CreateListItem(
          "Out of Office Action Items",
          logonUser.Title + " - " + updateTxtTitle.Text,
          logonUser.Id,
          EncodeDateTime(updateTxtStartDate.Text, updateTxtStartTime.Text, false),
          EncodeDateTime(updateTxtEndDate.Text, updateTxtEndTime.Text, true),
          updateTxtDescription.Text,
          updateDdlDepartment.SelectedValue,
          updateAllDayEvent.Checked,
          updateRecurringChb.Checked,
          WrapRecurringXML(),
          GetEventType(updateRecurringChb.Checked));

        EncodeRecurrenceData(newItem);
        newItem.EventDate = newItem.EventDate.ToUniversalTime();

        logonUser.EmailUser(logonUser.Manager,
          "<h1>Item Created</h1>" +
          "<p>Title: " + newItem.Title +
          "<br>Department: " + newItem.Department +
          "<br>Recur: " + newItem.DecodeRecurrenceData() +
          "<br>Start Date: " + newItem.EventDate.ToString("MMMM d, yyyy h:mm tt") +
          "<br>End Date: " + DoctorEndDate(newItem).ToString("MMMM d, yyyy h:mm tt") +
          "<br>Description: " + newItem.Description +
          "</p><p>Click <a href='http://ooom.clearwatercounty.ca/'>here</a> to approve or reject this item.</p>");

        Response.Redirect("/", false);
      }
      else
        SetJavaScript("update");
    }
    catch (Exception exception)
    {
      WriteException("Default.ClickCreateSuccess", exception.ToString());
    }
  }

  protected void ClickApproveSuccess(object sender, EventArgs e)
  {
    try
    {
      LinkButton linkButton = (LinkButton)sender;
      int id = Int32.Parse(linkButton.CommandArgument);

      int userID = ((Microsoft.SharePoint.Client.FieldUserValue)logonUser.ActionItems[id].Person).LookupId;
      string toSAMAccountName = "itservices";
      foreach (SharePointUser report in directReports)
      {
        if (report.Id == userID)
        {
          toSAMAccountName = report.SAMAccountName;
          break;
        }
      }

      logonUser.EmailUser(toSAMAccountName,
        "<h1>Item Approved</h1>" +
        "<p>Title: " + logonUser.ActionItems[id].Title +
        "<br>Department: " + logonUser.ActionItems[id].Department +
        "<br>Recur: " + logonUser.ActionItems[id].DecodeRecurrenceData() +
        "<br>Start Date: " + logonUser.ActionItems[id].EventDate.ToString("MMMM d, yyyy h:mm tt") +
        "<br>End Date: " + DoctorEndDate(logonUser.ActionItems[id]).ToString("MMMM d, yyyy h:mm tt") +
        "<br>Description: " + logonUser.ActionItems[id].Description + "</p>");

      logonUser.ApproveListItem(id);

      Response.Redirect("/", false);
    }
    catch (Exception exception)
    {
      WriteException("Default.ClickApproveSuccess", exception.ToString());
    }
  }

  protected void ClickRejectSuccess(object sender, EventArgs e)
  {
    try
    {
      LinkButton linkButton = (LinkButton)sender;
      int id = Int32.Parse(linkButton.CommandArgument);

      int userID = ((Microsoft.SharePoint.Client.FieldUserValue)logonUser.ActionItems[id].Person).LookupId;
      string toSAMAccountName = "itservices";
      foreach (SharePointUser report in directReports)
      {
        if (report.Id == userID)
        {
          toSAMAccountName = report.SAMAccountName;
          break;
        }
      }

      logonUser.EmailUser(toSAMAccountName,
        "<h1>Item Rejected</h1>" +
        "<p>Title: " + logonUser.ActionItems[id].Title +
        "<br>Department: " + logonUser.ActionItems[id].Department +
        "<br>Recur: " + logonUser.ActionItems[id].DecodeRecurrenceData() +
        "<br>Start Date: " + logonUser.ActionItems[id].EventDate.ToString("MMMM d, yyyy h:mm tt") +
        "<br>End Date: " + DoctorEndDate(logonUser.ActionItems[id]).ToString("MMMM d, yyyy h:mm tt") +
        "<br>Description: " + logonUser.ActionItems[id].Description +
        "<br>Reason: " + deleteWhy.Text + "</p>");

      logonUser.RejectListItem(id);

      Response.Redirect("/", false);
    }
    catch (Exception exception)
    {
      WriteException("Default.ClickRejectSuccess", exception.ToString());
    }
  }

  protected void ClickAllDayEvent(object sender, EventArgs e)
  {
    PreserveTextBox();
  }

  protected void ClickClose(object sender, EventArgs e)
  {
    Response.Redirect("/", false);
  }

  protected void ClickRecurring(object sender, EventArgs e)
  {
    updateRecurringRbl.ClearSelection();
    updateRecurringRblRange.ClearSelection();

    // Daily
    updateRecurringDailyRbl.Visible = false;
    updateRecurringDailyTxtDays.Visible = false;

    // Weekly
    updateRecurringWeeklyLblRecur.Visible = false;
    updateRecurringWeeklyTxtWeek.Visible = false;
    updateRecurringWeeklyChbDay.Visible = false;

    // Monthly
    updateRecurringRblMonthly.Visible = false;
    updateRecurringMonthlyTxtDay.Visible = false;
    updateRecurringMonthlyTxtMonths.Visible = false;
    updateRecurringMonthlyDdlWeek.Visible = false;
    updateRecurringMonthlyDdlDay2.Visible = false;
    updateRecurringMonthlyTxtMonths2.Visible = false;

    // Yearly
    updateRecurringYearlyRbl.Visible = false;
    updateRecurringYearlyDdlMonth.Visible = false;
    updateRecurringYearlyTxtDay.Visible = false;
    updateRecurringYearlyDdlWeek.Visible = false;
    updateRecurringYearlyDdlDay.Visible = false;
    updateRecurringYearlyDdlMonth2.Visible = false;

    // Range
    updateRecurringLblOccurrences.Visible = false;
    updateRecurringTxtOccurrences.Visible = false;
    updateRecurringLblEndDate.Visible = false;
    updateRecurringTxtEndDate.Visible = false;

    PreserveTextBox(true);
  }

  protected void ClickRecurringRbl(object sender, EventArgs e)
  {
    if (updateRecurringRbl.SelectedIndex == 0)
    {
      // Daily
      updateRecurringDailyRbl.ClearSelection();
      updateRecurringDailyRbl.Visible = true;
      updateRecurringDailyTxtDays.Visible = false;

      // Weekly
      updateRecurringWeeklyLblRecur.Visible = false;
      updateRecurringWeeklyTxtWeek.Visible = false;
      updateRecurringWeeklyChbDay.Visible = false;

      // Monthly
      updateRecurringRblMonthly.Visible = false;
      updateRecurringMonthlyTxtDay.Visible = false;
      updateRecurringMonthlyTxtMonths.Visible = false;
      updateRecurringMonthlyDdlWeek.Visible = false;
      updateRecurringMonthlyDdlDay2.Visible = false;
      updateRecurringMonthlyTxtMonths2.Visible = false;

      // Yearly
      updateRecurringYearlyRbl.Visible = false;
      updateRecurringYearlyDdlMonth.Visible = false;
      updateRecurringYearlyTxtDay.Visible = false;
      updateRecurringYearlyDdlWeek.Visible = false;
      updateRecurringYearlyDdlDay.Visible = false;
      updateRecurringYearlyDdlMonth2.Visible = false;
    }
    else if (updateRecurringRbl.SelectedIndex == 1)
    {
      // Daily
      updateRecurringDailyRbl.Visible = false;
      updateRecurringDailyTxtDays.Visible = false;

      // Weekly
      updateRecurringWeeklyLblRecur.Visible = true;
      updateRecurringWeeklyTxtWeek.Visible = true;
      updateRecurringWeeklyChbDay.Visible = true;

      // Monthly
      updateRecurringRblMonthly.Visible = false;
      updateRecurringMonthlyTxtDay.Visible = false;
      updateRecurringMonthlyTxtMonths.Visible = false;
      updateRecurringMonthlyDdlWeek.Visible = false;
      updateRecurringMonthlyDdlDay2.Visible = false;
      updateRecurringMonthlyTxtMonths2.Visible = false;

      // Yearly
      updateRecurringYearlyRbl.Visible = false;
      updateRecurringYearlyDdlMonth.Visible = false;
      updateRecurringYearlyTxtDay.Visible = false;
      updateRecurringYearlyDdlWeek.Visible = false;
      updateRecurringYearlyDdlDay.Visible = false;
      updateRecurringYearlyDdlMonth2.Visible = false;
    }
    else if (updateRecurringRbl.SelectedIndex == 2)
    {
      // Daily
      updateRecurringDailyRbl.Visible = false;
      updateRecurringDailyTxtDays.Visible = false;

      // Weekly
      updateRecurringWeeklyLblRecur.Visible = false;
      updateRecurringWeeklyTxtWeek.Visible = false;
      updateRecurringWeeklyChbDay.Visible = false;

      // Monthly
      updateRecurringRblMonthly.ClearSelection();
      updateRecurringRblMonthly.Visible = true;
      updateRecurringMonthlyTxtDay.Visible = false;
      updateRecurringMonthlyTxtMonths.Visible = false;
      updateRecurringMonthlyDdlWeek.Visible = false;
      updateRecurringMonthlyDdlDay2.Visible = false;
      updateRecurringMonthlyTxtMonths2.Visible = false;

      // Yearly
      updateRecurringYearlyRbl.Visible = false;
      updateRecurringYearlyDdlMonth.Visible = false;
      updateRecurringYearlyTxtDay.Visible = false;
      updateRecurringYearlyDdlWeek.Visible = false;
      updateRecurringYearlyDdlDay.Visible = false;
      updateRecurringYearlyDdlMonth2.Visible = false;
    }
    else if (updateRecurringRbl.SelectedIndex == 3)
    {
      // Daily
      updateRecurringDailyRbl.Visible = false;
      updateRecurringDailyTxtDays.Visible = false;

      // Weekly
      updateRecurringWeeklyLblRecur.Visible = false;
      updateRecurringWeeklyTxtWeek.Visible = false;
      updateRecurringWeeklyChbDay.Visible = false;

      // Monthly
      updateRecurringRblMonthly.Visible = false;
      updateRecurringMonthlyTxtDay.Visible = false;
      updateRecurringMonthlyTxtMonths.Visible = false;
      updateRecurringMonthlyDdlWeek.Visible = false;
      updateRecurringMonthlyDdlDay2.Visible = false;
      updateRecurringMonthlyTxtMonths2.Visible = false;

      // Yearly
      updateRecurringYearlyRbl.ClearSelection();
      updateRecurringYearlyRbl.Visible = true;
      updateRecurringYearlyDdlMonth.Visible = false;
      updateRecurringYearlyTxtDay.Visible = false;
      updateRecurringYearlyDdlWeek.Visible = false;
      updateRecurringYearlyDdlDay.Visible = false;
      updateRecurringYearlyDdlMonth2.Visible = false;
    }

    SetJavaScript("update");
  }

  protected void ClickRecurringRange(object sender, EventArgs e)
  {
    if (updateRecurringRblRange.SelectedIndex == 0)
    {
      updateRecurringLblOccurrences.Visible = false;
      updateRecurringTxtOccurrences.Visible = false;
      updateRecurringLblEndDate.Visible = false;
      updateRecurringTxtEndDate.Visible = false;
    }
    else if (updateRecurringRblRange.SelectedIndex == 1)
    {
      updateRecurringLblOccurrences.Visible = true;
      updateRecurringTxtOccurrences.Visible = true;
      updateRecurringLblEndDate.Visible = false;
      updateRecurringTxtEndDate.Visible = false;
    }
    else if (updateRecurringRblRange.SelectedIndex == 2)
    {
      updateRecurringLblOccurrences.Visible = false;
      updateRecurringTxtOccurrences.Visible = false;
      updateRecurringLblEndDate.Visible = true;
      updateRecurringTxtEndDate.Visible = true;
    }

    SetJavaScript("update");
  }

  protected void ClickRecurringDaily(object sender, EventArgs e)
  {
    if (updateRecurringDailyRbl.SelectedIndex == 0)
      updateRecurringDailyTxtDays.Visible = true;
    else if (updateRecurringDailyRbl.SelectedIndex == 1)
      updateRecurringDailyTxtDays.Visible = false;

    SetJavaScript("update");
  }

  protected void ClickRecurringMonthly(object sender, EventArgs e)
  {
    if (updateRecurringRblMonthly.SelectedIndex == 0)
    {
      updateRecurringMonthlyTxtDay.Visible = true;
      updateRecurringMonthlyTxtMonths.Visible = true;

      updateRecurringMonthlyDdlWeek.Visible = false;
      updateRecurringMonthlyDdlDay2.Visible = false;
      updateRecurringMonthlyTxtMonths2.Visible = false;
    }
    else if (updateRecurringRblMonthly.SelectedIndex == 1)
    {
      updateRecurringMonthlyTxtDay.Visible = false;
      updateRecurringMonthlyTxtMonths.Visible = false;

      updateRecurringMonthlyDdlWeek.Visible = true;
      updateRecurringMonthlyDdlDay2.Visible = true;
      updateRecurringMonthlyTxtMonths2.Visible = true;
    }

    SetJavaScript("update");
  }

  protected void ClickRecurringYealy(object sender, EventArgs e)
  {
    if (updateRecurringYearlyRbl.SelectedIndex == 0)
    {
      updateRecurringYearlyDdlMonth.Visible = true;
      updateRecurringYearlyTxtDay.Visible = true;

      updateRecurringYearlyDdlWeek.Visible = false;
      updateRecurringYearlyDdlDay.Visible = false;
      updateRecurringYearlyDdlMonth2.Visible = false;
    }
    else if (updateRecurringYearlyRbl.SelectedIndex == 1)
    {
      updateRecurringYearlyDdlMonth.Visible = false;
      updateRecurringYearlyTxtDay.Visible = false;

      updateRecurringYearlyDdlWeek.Visible = true;
      updateRecurringYearlyDdlDay.Visible = true;
      updateRecurringYearlyDdlMonth2.Visible = true;
    }

    SetJavaScript("update");
  }

  private void SetJavaScript(string id)
  {
    try
    {
      string startupScript = "";
      switch (updateRecurringChb.Checked)
      {
        case true:
          startupScript = "$('#row3').hide();$('#row1').show();$('#row2').show();";
          break;
        case false:
          startupScript = "$('#row1').hide();$('#row2').hide();$('#row3').show();";
          break;
      }
      ClientScript.RegisterStartupScript(this.GetType(), "JavaScript", "$('#" + id + "').foundation('open').appendTo($('#form'));" + startupScript, true);
    }
    catch (Exception exception)
    {
      WriteException("Default.SetJavaScript", exception.ToString());
    }
  }

  private void PreserveTextBox(bool calledByRecurring = false)
  {
    try
    {
      switch (updateAllDayEvent.Checked)
      {
        case true:
          updateTxtStartTime.Enabled = false;
          updateTxtEndTime.Enabled = false;
          updateTxtStartTime.Text = "00:00:00";
          updateTxtEndTime.Text = "23:59:00";

          if (updateRecurringChb.Checked)
          {
            updateTxtEndDate.Enabled = false;
            updateTxtEndDate.Text = DateTime.Now.AddDays(1397).ToString("yyyy-MM-dd");
          }
          else updateTxtEndDate.Enabled = true;
          break;

        case false:
          updateTxtStartTime.Enabled = true;
          updateTxtEndTime.Enabled = true;
          updateTxtEndDate.Enabled = true;
          updateTxtEndDate.Text = "";
          
          if (!calledByRecurring)
          {
            updateTxtStartTime.Text = "";
            updateTxtEndTime.Text = "";
          }
          break;
      }

      SetJavaScript("update");
    }
    catch (Exception exception)
    {
      WriteException("Default.PreserveTextBox", exception.ToString());
    }
  }

  public DateTime EncodeDateTime(string date, string time, bool endDate)
  {
    try
    {
      if (updateAllDayEvent.Checked && updateRecurringChb.Checked && endDate)
      {
        DateTime end = Convert.ToDateTime(EncodeDateTime(updateTxtStartDate.Text, updateTxtStartTime.Text, false)).AddDays(1397);
        updateTxtEndDate.Text = end.ToString("yyyy-MM-dd");
        return new DateTime(end.Year, end.Month, end.Day, 23, 59, 00).ToLocalTime();
      }
      else
      {
        int[] dateSplit = Array.ConvertAll(date.Split('-'), int.Parse);
        int[] timeSplit = Array.ConvertAll(time.Split(':'), int.Parse);
        if (updateAllDayEvent.Checked) return new DateTime(dateSplit[0], dateSplit[1], dateSplit[2], timeSplit[0], timeSplit[1], timeSplit[2]).ToLocalTime();
        else return new DateTime(dateSplit[0], dateSplit[1], dateSplit[2], timeSplit[0], timeSplit[1], timeSplit[2]).ToUniversalTime();
      }
    }
    catch (Exception exception)
    {
      WriteException("SharePointUser.EncodeDateTime", exception.ToString());
      return new DateTime();
    }
  }

  private void SetRecurControls(SharePointListItem item, object sender, EventArgs e)
  {
    try
    {
      if (item.FRecurrence)
      {
        updateRecurringChb.Checked = true;
        ClickRecurring(sender, e);

        if (item.Daily != null)
        {
          updateRecurringRbl.SelectedIndex = 0;
          ClickRecurringRbl(sender, e);

          if (item.DailyDayFrequency != null)
          {
            updateRecurringDailyRbl.SelectedIndex = 0;
            ClickRecurringDaily(sender, e);
            updateRecurringDailyTxtDays.Text = (string)item.DailyDayFrequency;
          }
          else if (item.DailyWeekday != null)
          {
            updateRecurringDailyRbl.SelectedIndex = 1;
            ClickRecurringDaily(sender, e);
          }
        }

        else if (item.Weekly != null)
        {
          updateRecurringRbl.SelectedIndex = 1;
          ClickRecurringRbl(sender, e);

          updateRecurringWeeklyTxtWeek.Text = (string)item.WeeklyWeekFrequency;
          if (item.WeeklySu != null) updateRecurringWeeklyChbDay.Items.FindByValue("su").Selected = true;
          if (item.WeeklyMo != null) updateRecurringWeeklyChbDay.Items.FindByValue("mo").Selected = true;
          if (item.WeeklyTu != null) updateRecurringWeeklyChbDay.Items.FindByValue("tu").Selected = true;
          if (item.WeeklyWe != null) updateRecurringWeeklyChbDay.Items.FindByValue("we").Selected = true;
          if (item.WeeklyTh != null) updateRecurringWeeklyChbDay.Items.FindByValue("th").Selected = true;
          if (item.WeeklyFr != null) updateRecurringWeeklyChbDay.Items.FindByValue("fr").Selected = true;
          if (item.WeeklySa != null) updateRecurringWeeklyChbDay.Items.FindByValue("sa").Selected = true;
        }

        else if (item.Monthly != null)
        {
          updateRecurringRbl.SelectedIndex = 2;
          ClickRecurringRbl(sender, e);
          updateRecurringRblMonthly.SelectedIndex = 0;
          ClickRecurringMonthly(sender, e);


          updateRecurringMonthlyTxtDay.Text = (string)item.MonthlyDay;
          updateRecurringMonthlyTxtMonths.Text = (string)item.MonthlyMonthFrequency;
        }

        else if (item.MonthlyByDay != null)
        {
          updateRecurringRbl.SelectedIndex = 2;
          ClickRecurringRbl(sender, e);
          updateRecurringRblMonthly.SelectedIndex = 1;
          ClickRecurringMonthly(sender, e);

          updateRecurringMonthlyDdlWeek.SelectedValue = (string)item.MonthlyByDayWeekdayOfMonth;
          if (item.MonthlyByDaySu != null) updateRecurringMonthlyDdlDay2.Items[1].Selected = true;
          else if (item.MonthlyByDayMo != null) updateRecurringMonthlyDdlDay2.Items[2].Selected = true;
          else if (item.MonthlyByDayTu != null) updateRecurringMonthlyDdlDay2.Items[3].Selected = true;
          else if (item.MonthlyByDayWe != null) updateRecurringMonthlyDdlDay2.Items[4].Selected = true;
          else if (item.MonthlyByDayTh != null) updateRecurringMonthlyDdlDay2.Items[5].Selected = true;
          else if (item.MonthlyByDayFr != null) updateRecurringMonthlyDdlDay2.Items[6].Selected = true;
          else if (item.MonthlyByDaySa != null) updateRecurringMonthlyDdlDay2.Items[7].Selected = true;
          else if (item.MonthlyByDayDay != null) updateRecurringMonthlyDdlDay2.Items[8].Selected = true;
          else if (item.MonthlyByDayWeekday != null) updateRecurringMonthlyDdlDay2.Items[9].Selected = true;
          else if (item.MonthlyByDayWeekend_day != null) updateRecurringMonthlyDdlDay2.Items[10].Selected = true;
          updateRecurringMonthlyTxtMonths2.Text = (string)item.MonthlyByDayMonthFrequency;
        }

        else if (item.Yearly != null)
        {
          updateRecurringRbl.SelectedIndex = 3;
          ClickRecurringRbl(sender, e);
          updateRecurringYearlyRbl.SelectedIndex = 0;
          ClickRecurringYealy(sender, e);

          updateRecurringYearlyDdlMonth.Items[Convert.ToInt32(item.YearlyMonth)].Selected = true;
          updateRecurringYearlyTxtDay.Text = (string)item.YearlyDay;
        }

        else if (item.YearlyByDay != null)
        {
          updateRecurringRbl.SelectedIndex = 3;
          ClickRecurringRbl(sender, e);
          updateRecurringYearlyRbl.SelectedIndex = 1;
          ClickRecurringYealy(sender, e);

          updateRecurringYearlyDdlWeek.Items.FindByValue((string)item.YearlyByDayWeekdayOfMonth).Selected = true;
          if (item.YearlyByDaySu != null) updateRecurringYearlyDdlDay.Items[1].Selected = true;
          else if (item.YearlyByDayMo != null) updateRecurringYearlyDdlDay.Items[2].Selected = true;
          else if (item.YearlyByDayTu != null) updateRecurringYearlyDdlDay.Items[3].Selected = true;
          else if (item.YearlyByDayWe != null) updateRecurringYearlyDdlDay.Items[4].Selected = true;
          else if (item.YearlyByDayTh != null) updateRecurringYearlyDdlDay.Items[5].Selected = true;
          else if (item.YearlyByDayFr != null) updateRecurringYearlyDdlDay.Items[6].Selected = true;
          else if (item.YearlyByDaySa != null) updateRecurringYearlyDdlDay.Items[7].Selected = true;
          else if (item.YearlyByDayDay != null) updateRecurringYearlyDdlDay.Items[8].Selected = true;
          else if (item.YearlyByDayWeekday != null) updateRecurringYearlyDdlDay.Items[9].Selected = true;
          else if (item.YearlyByDayWeekend_day != null) updateRecurringYearlyDdlDay.Items[10].Selected = true;
          updateRecurringYearlyDdlMonth2.Items[Convert.ToInt32(item.YearlyByDayMonth)].Selected = true;
        }

        if (item.RepeatForever != null)
        {
          updateRecurringRblRange.SelectedIndex = 0;
          ClickRecurringRange(sender, e);
        }
        else if (item.RepeatInstances != null)
        {
          updateRecurringRblRange.SelectedIndex = 1;
          ClickRecurringRange(sender, e);
          updateRecurringTxtOccurrences.Text = (string)item.RepeatInstances;
        }
        else if (item.WindowEnd != null)
        {
          updateRecurringRblRange.SelectedIndex = 2;
          ClickRecurringRange(sender, e);
          updateRecurringTxtEndDate.Text = Convert.ToDateTime(item.WindowEnd).ToString("yyyy-MM-dd");
        }
      }
    }
    catch (Exception exception)
    {
      WriteException("Default.SetRecurControls", exception.ToString());
    }
  }

  private void EncodeRecurrenceData(SharePointListItem item)
  {
    try
    {
      // Convert recurrence data to human language
      if (updateRecurringChb.Checked)
      {
        item.FirstDayOfWeek = null;

        item.Daily = null;
        item.DailyDayFrequency = null;
        item.DailyWeekday = null;

        item.Weekly = null;
        item.WeeklyWeekFrequency = null;
        item.WeeklySu = null;
        item.WeeklyMo = null;
        item.WeeklyTu = null;
        item.WeeklyWe = null;
        item.WeeklyTh = null;
        item.WeeklyFr = null;
        item.WeeklySa = null;

        item.Monthly = null;
        item.MonthlyDay = null;
        item.MonthlyMonthFrequency = null;

        item.MonthlyByDay = null;
        item.MonthlyByDayMonthFrequency = null;
        item.MonthlyByDaySu = null;
        item.MonthlyByDayMo = null;
        item.MonthlyByDayTu = null;
        item.MonthlyByDayWe = null;
        item.MonthlyByDayTh = null;
        item.MonthlyByDayFr = null;
        item.MonthlyByDaySa = null;
        item.MonthlyByDayDay = null;
        item.MonthlyByDayWeekday = null;
        item.MonthlyByDayWeekend_day = null;
        item.MonthlyByDayWeekdayOfMonth = null;

        item.Yearly = null;
        item.YearlyYearFrequency = null;
        item.YearlyMonth = null;
        item.YearlyDay = null;

        item.YearlyByDay = null;
        item.YearlyByDayYearFrequency = null;
        item.YearlyByDayMonth = null;
        item.YearlyByDaySu = null;
        item.YearlyByDayMo = null;
        item.YearlyByDayTu = null;
        item.YearlyByDayWe = null;
        item.YearlyByDayTh = null;
        item.YearlyByDayFr = null;
        item.YearlyByDaySa = null;
        item.YearlyByDayDay = null;
        item.YearlyByDayWeekday = null;
        item.YearlyByDayWeekend_day = null;
        item.YearlyByDayWeekdayOfMonth = null;

        item.RepeatInstances = null;
        item.WindowEnd = null;
        item.RepeatForever = null;

        switch (updateRecurringRbl.SelectedIndex)
        {
          // Daily
          case 0:
            item.Daily = true;
            if (updateRecurringDailyRbl.SelectedIndex == 0)
              item.DailyDayFrequency = updateRecurringDailyTxtDays.Text;
            else if (updateRecurringDailyRbl.SelectedIndex == 1)
              item.DailyWeekday = true;
            break;

          // Weekly
          case 1:
            item.Weekly = true;
            item.WeeklyWeekFrequency = updateRecurringWeeklyTxtWeek.Text;
            if (updateRecurringWeeklyChbDay.Items[0].Selected) item.WeeklySu = true;
            if (updateRecurringWeeklyChbDay.Items[1].Selected) item.WeeklyMo = true;
            if (updateRecurringWeeklyChbDay.Items[2].Selected) item.WeeklyTu = true;
            if (updateRecurringWeeklyChbDay.Items[3].Selected) item.WeeklyWe = true;
            if (updateRecurringWeeklyChbDay.Items[4].Selected) item.WeeklyTh = true;
            if (updateRecurringWeeklyChbDay.Items[5].Selected) item.WeeklyFr = true;
            if (updateRecurringWeeklyChbDay.Items[6].Selected) item.WeeklySa = true;
            break;

          // Monthly
          case 2:
            switch (updateRecurringRblMonthly.SelectedIndex)
            {
              case 0:
                item.Monthly = true;
                item.MonthlyDay = updateRecurringMonthlyTxtDay.Text;
                item.MonthlyMonthFrequency = updateRecurringMonthlyTxtMonths.Text;
                break;

              case 1:
                item.MonthlyByDay = true;
                item.MonthlyByDayWeekdayOfMonth = updateRecurringMonthlyDdlWeek.SelectedValue;
                switch (updateRecurringMonthlyDdlDay2.SelectedIndex)
                {
                  case 1: item.MonthlyByDaySu = true; break;
                  case 2: item.MonthlyByDayMo = true; break;
                  case 3: item.MonthlyByDayTu = true; break;
                  case 4: item.MonthlyByDayWe = true; break;
                  case 5: item.MonthlyByDayTh = true; break;
                  case 6: item.MonthlyByDayFr = true; break;
                  case 7: item.MonthlyByDaySa = true; break;
                  case 8: item.MonthlyByDayDay = true; break;
                  case 9: item.MonthlyByDayWeekday = true; break;
                  case 10: item.MonthlyByDayWeekend_day = true; break;
                }
                item.MonthlyByDayMonthFrequency = updateRecurringMonthlyTxtMonths2.Text;
                break;
            }
            break;

          // Yearly
          case 3:
            switch (updateRecurringYearlyRbl.SelectedIndex)
            {
              case 0:
                item.Yearly = true;
                item.YearlyDay = updateRecurringYearlyTxtDay.Text;
                item.YearlyMonth = updateRecurringYearlyDdlMonth.SelectedIndex;
                break;

              case 1:
                item.YearlyByDay = true;
                item.YearlyByDayWeekdayOfMonth = updateRecurringYearlyDdlWeek.SelectedValue;
                switch (updateRecurringYearlyDdlDay.SelectedIndex)
                {
                  case 1: item.YearlyByDaySu = true; break;
                  case 2: item.YearlyByDayMo = true; break;
                  case 3: item.YearlyByDayTu = true; break;
                  case 4: item.YearlyByDayWe = true; break;
                  case 5: item.YearlyByDayTh = true; break;
                  case 6: item.YearlyByDayFr = true; break;
                  case 7: item.YearlyByDaySa = true; break;
                  case 8: item.YearlyByDayDay = true; break;
                  case 9: item.YearlyByDayWeekday = true; break;
                  case 10: item.YearlyByDayWeekend_day = true; break;
                }
                item.YearlyByDayMonth = updateRecurringYearlyDdlMonth2.SelectedIndex;
                break;
            }
            break;
        }

        item.EventDate = EncodeDateTime(updateTxtStartDate.Text, updateTxtStartTime.Text, false);
        switch (updateRecurringRblRange.SelectedIndex)
        {
          case 0:
            item.RepeatForever = false;
            break;

          case 1:
            item.RepeatInstances = updateRecurringTxtOccurrences.Text;
            break;

          case 2:
            item.WindowEnd = EncodeDateTime(updateRecurringTxtEndDate.Text, "23:59:00", false).ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssZ");
            break;
        }
      }
    }
    catch (Exception exception)
    {
      WriteException("Default.EncodeRecurrenceData", exception.ToString());
    }
  }

  public string WrapRecurringXML()
  {
    try
    {
      // Convert recurrence data to human language
      string output = null;
      if (updateRecurringChb.Checked)
      {
        output += "<recurrence><rule><firstDayOfWeek>su</firstDayOfWeek><repeat>";
        switch (updateRecurringRbl.SelectedIndex)
        {
          // Daily
          case 0:
            output += "<daily";
            if (updateRecurringDailyRbl.SelectedIndex == 0) output += " dayFrequency=\"" + updateRecurringDailyTxtDays.Text + "\"";
            else if (updateRecurringDailyRbl.SelectedIndex == 1) output += " weekday=\"TRUE\"";
            output += " />";
            break;

          // Weekly
          case 1:
            output += "<weekly weekFrequency=\"" + updateRecurringWeeklyTxtWeek.Text + "\"";
            if (updateRecurringWeeklyChbDay.Items[0].Selected) output += " su=\"TRUE\"";
            if (updateRecurringWeeklyChbDay.Items[1].Selected) output += " mo=\"TRUE\"";
            if (updateRecurringWeeklyChbDay.Items[2].Selected) output += " tu=\"TRUE\"";
            if (updateRecurringWeeklyChbDay.Items[3].Selected) output += " we=\"TRUE\"";
            if (updateRecurringWeeklyChbDay.Items[4].Selected) output += " th=\"TRUE\"";
            if (updateRecurringWeeklyChbDay.Items[5].Selected) output += " fr=\"TRUE\"";
            if (updateRecurringWeeklyChbDay.Items[6].Selected) output += " sa=\"TRUE\"";
            output += " />";
            break;

          // Monthly
          case 2:
            switch (updateRecurringRblMonthly.SelectedIndex)
            {
              case 0:
                output += "<monthly day=\"" + updateRecurringMonthlyTxtDay.Text +
                  "\" monthFrequency=\"" + updateRecurringMonthlyTxtMonths.Text + "\" />";
                break;

              case 1:
                output += "<monthlyByDay weekdayOfMonth=\"" + updateRecurringMonthlyDdlWeek.SelectedValue + "\"";
                switch (updateRecurringMonthlyDdlDay2.SelectedIndex)
                {
                  case 1: output += " su=\"TRUE\""; break;
                  case 2: output += " mo=\"TRUE\""; break;
                  case 3: output += " tu=\"TRUE\""; break;
                  case 4: output += " we=\"TRUE\""; break;
                  case 5: output += " th=\"TRUE\""; break;
                  case 6: output += " fr=\"TRUE\""; break;
                  case 7: output += " sa=\"TRUE\""; break;
                  case 8: output += " day=\"TRUE\""; break;
                  case 9: output += " weekday=\"TRUE\""; break;
                  case 10: output += " weekend_day=\"TRUE\""; break;
                }
                output += " monthFrequency=\"" + updateRecurringMonthlyTxtMonths2.Text + "\" />";
                break;
            }
            break;

          // Yearly
          case 3:
            switch (updateRecurringYearlyRbl.SelectedIndex)
            {
              case 0:
                output += "<yearly day=\"" + updateRecurringYearlyTxtDay.Text +
                  "\" month=\"" + updateRecurringYearlyDdlMonth.SelectedIndex + "\" yearFrequency=\"1\" />";
                break;

              case 1:
                output += "<yearlyByDay yearFrequency=\"1\" weekdayOfMonth=\"" + updateRecurringYearlyDdlWeek.SelectedValue + "\"";
                switch (updateRecurringYearlyDdlDay.SelectedIndex)
                {
                  case 1: output += " su=\"TRUE\""; break;
                  case 2: output += " mo=\"TRUE\""; break;
                  case 3: output += " tu=\"TRUE\""; break;
                  case 4: output += " we=\"TRUE\""; break;
                  case 5: output += " th=\"TRUE\""; break;
                  case 6: output += " fr=\"TRUE\""; break;
                  case 7: output += " sa=\"TRUE\""; break;
                  case 8: output += " day=\"TRUE\""; break;
                  case 9: output += " weekday=\"TRUE\""; break;
                  case 10: output += " weekend_day=\"TRUE\""; break;
                }
                output += " month=\"" + updateRecurringYearlyDdlMonth2.SelectedIndex + "\" />";
                break;
            }
            break;
        }
        output += "</repeat>";

        switch (updateRecurringRblRange.SelectedIndex)
        {
          case 0:
            output += "<repeatForever>FALSE</repeatForever>";
            break;

          case 1:
            output += "<repeatInstances>" + updateRecurringTxtOccurrences.Text + "</repeatInstances>";
            break;

          case 2:
            output += "<windowEnd>" + EncodeDateTime(updateRecurringTxtEndDate.Text, "23:59:00", false).ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssZ") + "</windowEnd>";
            break;
        }
        output += "</rule></recurrence>";
      }
      return output;
    }
    catch (Exception exception)
    {
      WriteException("SharePointUser.WrapRecurringXML", exception.ToString());
      return null;
    }
  }

  public int GetEventType(bool recuring)
  {
    if (recuring) return 1;
    else return 0;
  }

  private bool ValidateUpdateForm()
  {
    try
    {
      string body = "";

      // Check title
      if (String.IsNullOrEmpty(updateTxtTitle.Text)) body += "Title is empty.<br>";

      // Check department
      if (updateDdlDepartment.SelectedIndex == 0) body += "Department is empty.<br>";

      // Check start time
      DateTime startDT = new DateTime();
      bool startDTFlag = true;
      if (String.IsNullOrEmpty(updateTxtStartTime.Text))
      {
        body += "Start Time is empty.<br>";
        startDTFlag = false;
      }

      if (String.IsNullOrEmpty(updateTxtStartDate.Text))
      {
        body += "Start Date is empty.<br>";
        startDTFlag = false;
      }

      if (startDTFlag)
      {
        startDT = EncodeDateTime(updateTxtStartDate.Text, updateTxtStartTime.Text, false);
        if (startDT <= DateTime.Now)
          body += "Start date is in the past.<br>";
      }

      // Check end time
      DateTime endDT = new DateTime();
      bool endDTFlag = true;
      if (updateTxtEndTime.Text == "")
      {
        body += "End Time is empty.<br>";
        endDTFlag = false;
      }

      if (updateTxtEndDate.Text == "" && (!updateAllDayEvent.Checked || !updateRecurringChb.Checked))
      {
        body += "End Date is empty.<br>";
        endDTFlag = false;
      }

      if (endDTFlag && startDTFlag)
      {
        endDT = EncodeDateTime(updateTxtEndDate.Text, updateTxtEndTime.Text, true);
        if (endDT <= startDT)
          body += "End date/time is before start date/time<br>";
      }

      // Recurring event
      if (updateRecurringChb.Checked)
      {
        // Daily, weekly, monthly, or yealy
        switch (updateRecurringRbl.SelectedIndex)
        {
          // Daily
          case 0:
            int number; // numberic value from textboxes
            bool canConvert; // Test numberic value in textboxs

            switch (updateRecurringDailyRbl.SelectedIndex)
            {
              // Every X day(s)
              case 0:
                number = 0;
                canConvert = int.TryParse(updateRecurringDailyTxtDays.Text.Trim(), out number);

                // Check days
                if (string.IsNullOrEmpty(updateRecurringDailyTxtDays.Text)) body += "X is empty.<br>";
                else if (!canConvert) body += "X is not numeric.<br>";
                else if (number < 1) body += "X is less than 1.<br>";
                break;

              // Every Weekday
              case 1:
                break;

              default:
                body += "Daily frequency is not selected.<br>";
                break;
            }
            break;

          // Weekly
          case 1:
            number = 0;
            canConvert = int.TryParse(updateRecurringWeeklyTxtWeek.Text.Trim(), out number);
            if (string.IsNullOrEmpty(updateRecurringWeeklyTxtWeek.Text)) body += "X is empty.<br>";
            else if (!canConvert) body += "X is not numeric.<br>";
            else if (number < 1) body += "X is less than 1.<br>";
            if (updateRecurringWeeklyChbDay.SelectedIndex == -1) body += "Day of week not checked.<br>";
            break;

          // Monthly
          case 2:
            switch (updateRecurringRblMonthly.SelectedIndex)
            {
              // Day X of every Y of month(s)
              case 0:
                number = 0;
                canConvert = int.TryParse(updateRecurringMonthlyTxtDay.Text.Trim(), out number);
                if (string.IsNullOrEmpty(updateRecurringMonthlyTxtDay.Text)) body += "X is empty.<br>";
                else if (!canConvert) body += "X is not numeric.<br>";
                else if (number < 1) body += "X is less than 1.<br>";
                else if (number > 31) body += "X is greater than 31.<br>";

                number = 0;
                canConvert = int.TryParse(updateRecurringMonthlyTxtMonths.Text.Trim(), out number);
                if (string.IsNullOrEmpty(updateRecurringMonthlyTxtMonths.Text)) body += "Y is empty.<br>";
                else if (!canConvert) body += "Y is not numeric.<br>";
                else if (number < 1) body += "Y is less than 1.<br>";
                else if (number > 13) body += "Y is greater than 13.<br>";
                break;

              // Every X Y of every Z month(s)
              case 1:
                if (updateRecurringMonthlyDdlWeek.SelectedIndex < 1) body += "X is not selected.<br>";
                if (updateRecurringMonthlyDdlDay2.SelectedIndex < 1) body += "Y is not selected.<br>";

                number = 0;
                canConvert = int.TryParse(updateRecurringMonthlyTxtMonths2.Text.Trim(), out number);
                if (string.IsNullOrEmpty(updateRecurringMonthlyTxtMonths2.Text)) body += "Z is empty.<br>";
                else if (!canConvert) body += "Z is not numeric.<br>";
                else if (number < 1) body += "Z is less than 1.<br>";
                else if (number > 12) body += "Z is greater than 12.<br>";
                break;

              default:
                body += "Monthly frequency is not selected.<br>";
                break;
            }
            break;

          // Yearly
          case 3:
            switch (updateRecurringYearlyRbl.SelectedIndex)
            {
              // Every X Y
              case 0:
                if (updateRecurringYearlyDdlMonth.SelectedIndex < 1) body += "X is not selected.<br>";

                number = 0;
                canConvert = int.TryParse(updateRecurringYearlyTxtDay.Text.Trim(), out number);
                if (string.IsNullOrEmpty(updateRecurringYearlyTxtDay.Text)) body += "Y is empty.<br>";
                else if (!canConvert) body += "Y is not numeric.<br>";
                else if (number < 1) body += "Y is less than 1.<br>";
                else if (number > 31) body += "Y is greater than 31.<br>";
                break;

              // Every X Y of Z
              case 1:
                if (updateRecurringYearlyDdlWeek.SelectedIndex < 1) body += "X is not selected.<br>";
                if (updateRecurringYearlyDdlDay.SelectedIndex < 1) body += "Y is not selected.<br>";
                if (updateRecurringYearlyDdlMonth2.SelectedIndex < 1) body += "Z is not selected.<br>";
                break;

              default:
                body += "Yearly frequency is not selected.<br>";
                break;
            }
            break;

          default:
            body += "Frequency is not selected.<br>";
            break;
        } // End "Daily, weekly, monthly, or yealy"

        // Range
        switch (updateRecurringRblRange.SelectedIndex)
        {
          // No end date
          case 0:
            break;

          // End after X occurrence(s)
          case 1:
            int x = 0;
            bool canConvert = int.TryParse(updateRecurringTxtOccurrences.Text.Trim(), out x);
            if (String.IsNullOrEmpty(updateRecurringTxtOccurrences.Text)) body += "Occurrences is empty.<br>";
            else if (!canConvert) body += "Occurrences is not numeric.<br>";
            else if (x < 1) body += "Occurrences is less than 1.<br>";
            break;

          // End by specific date
          case 2:
            DateTime recurEndDate = Convert.ToDateTime(updateRecurringTxtEndDate.Text + " 23:59:00");
            if (updateRecurringTxtEndDate.Text == "") body += "Recur window end date is empty.<br>";
            else if (recurEndDate <= startDT && startDTFlag) body += "Recur window end date is before Start Time.<br>";
            break;

          default:
            body += "Range is not selected.<br>";
            break;
        } // End "Range"
      } // End "Recurring event"

      if (body != "")
      {
        formValidation.Text =
        "<div class='alert callout' data-closable>" +
          "<p>" + body + "</p>" +
        "</div>";
        formValidation.Visible = true;
        return false;
      }
    }
    catch (Exception exception)
    {
      WriteException("Default.ValidateForm", exception.ToString());
      return false;
    }
    return true;
  }

  public void WriteException(string h5, string p)
  {
    if (p.Length != 0)
    {
      // hh:mm:ss AM/PM ==> Log Message
      string sLogFormat = DateTime.Now.ToLongTimeString().ToString() + " ==> ";
      string path = System.Web.Hosting.HostingEnvironment.MapPath("~/Logs/");

      using (System.IO.StreamWriter w = System.IO.File.AppendText(path + DateTime.Now.ToString("yyyy-MM-dd") + ".log"))
        w.WriteLine(sLogFormat + "USER: " + logonUser.Title + ", TITLE: " + h5 + ", PARAGRAPH: " + p);

      logonUser.EmailUser("ITServices", "<p>An error has occurred. Logs have been generated on the host in " + path + DateTime.Now.ToString("yyyy-MM-dd") + ".log</p>" +
        "<p>Also hosted remotely at http://ooom.clearwatercounty.ca/Logs/" + DateTime.Now.ToString("yyyy-MM-dd") + ".log</p>");

      HttpContext.Current.Response.Redirect("Error.aspx");
    }
  }

  private DateTime DoctorEndDate(SharePointListItem item)
  {
    if (item.FAllDayEvent && item.FRecurrence)
      return new DateTime(item.EventDate.Year, item.EventDate.Month, item.EventDate.Day, 23, 59, 00);
    else
      return item.EndDate;
  }
}