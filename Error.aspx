﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Error.aspx.cs" Inherits="Error" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

  <!-- Metadata -->
  <meta charset="utf-8" />
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <title>Out of Office Manager</title>
  <link rel="icon" type="image/png" href="favicon.png" />

  <!-- CSS -->
  <link href="CSS/font-awesome.min.css" rel="stylesheet" />
  <link href="CSS/foundation.min.css" rel="stylesheet" />
  <link href="CSS/foundation-datepicker.min.css" rel="stylesheet" />

  <style>
    .center {
      position: fixed;
      top: 30%;
      left: 50%;
      transform: translate(-50%,-50%);
      text-align: center;
    }
  </style>
</head>
<body>
  <form id="form" runat="server">
    <div class="center">
      <i class="fa fa-thumbs-down fa-5x" aria-hidden="true"></i>
      <p>&nbsp;</p>
      <h2>Not today chief.</h2>
      <h5>An error has occurred<br />
        Your system administrator has been notified
      </h5>
    </div>
  </form>
</body>
</html>
