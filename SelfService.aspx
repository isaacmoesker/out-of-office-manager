﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SelfService.aspx.cs" Inherits="SelfService" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

  <!-- Metadata -->
  <meta charset="utf-8" />
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <title>Out of Office Manager</title>
  <link rel="icon" type="image/png" href="favicon.png" />

  <!-- CSS -->
  <link href="CSS/font-awesome.min.css" rel="stylesheet" />
  <link href="CSS/foundation.min.css" rel="stylesheet" />
  <link href="CSS/foundation-datepicker.min.css" rel="stylesheet" />

  <style>
    .center {
      position: fixed;
      top: 30%;
      left: 50%;
      transform: translate(-50%,-50%);
      text-align: center;
    }
  </style>
</head>
<body>
  <form id="form" runat="server">
    <asp:Label ID="lbl" runat="server" />
    <div class="center">
      <i class="fa fa-wrench fa-5x" aria-hidden="true"></i>
      <p>&nbsp;</p>
      <h5>Let's get a few things in order</h5>
      <p>&nbsp;</p>
      <asp:Label ID="lblCategory" AssociatedControlID="ddlItems" runat="server" />
      <asp:DropDownList ID="ddlItems" runat="server" />
      <asp:LinkButton ID="btnSubmitDepartment" CausesValidation="false"
        CssClass="button expanded"
        EnableViewState="true"
        OnClick="ClickDepartment"
        OnClientClick="$(this).html(&quot;<i class='fa fa-cog fa-spin fa-fw margin-bottom'></i><span class='sr-only'>Loading...</span>&quot;);"
        Text="Set"
        runat="server" />
      <asp:LinkButton ID="btnSubmitManager" CausesValidation="false"
        CssClass="button expanded"
        EnableViewState="true"
        OnClick="ClickManager"
        OnClientClick="$(this).html(&quot;<i class='fa fa-cog fa-spin fa-fw margin-bottom'></i><span class='sr-only'>Loading...</span>&quot;);"
        Text="Set"
        runat="server" />
    </div>
    <script src="JavaScript/jquery-2.2.2.min.js"></script>
  </form>
</body>
</html>
