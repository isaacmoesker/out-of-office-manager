﻿<%@ Page Language="C#" Async="True" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

  <!-- Metadata -->
  <meta charset="utf-8" />
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <title>Out of Office Manager</title>
  <link rel="icon" type="image/png" href="favicon.png" />

  <!-- CSS -->
  <link href="CSS/font-awesome.min.css" rel="stylesheet" />
  <link href="CSS/foundation.min.css" rel="stylesheet" />
  <link href="CSS/foundation-datepicker.min.css" rel="stylesheet" />
</head>
<body>
  <form id="form" runat="server">
    <!-- Action Items -->
    <asp:Panel ID="panelAction" CssClass="column row" Visible="false" runat="server">
      <div class="table-scroll">
        <asp:Table ID="tableAction" Caption="Action Items" CssClass="hover" Width="100%" runat="server">
          <asp:TableRow ID="tablerowActionItems" TableSection="TableHeader" runat="server">
            <asp:TableCell Text="Title" Width="30%" />
            <asp:TableCell Text="Start" Width="30%" />
            <asp:TableCell Text="End" Width="30%" />
            <asp:TableCell Text="Actions" Width="10%" HorizontalAlign="Center" />
          </asp:TableRow>
        </asp:Table>
      </div>
    </asp:Panel>

    <!-- My Items -->
    <div class="column row">
      <div class="table-scroll">
        <asp:Table ID="tableMy" Caption="My Items" CssClass="hover" Width="100%" runat="server">
          <asp:TableRow ID="tableMyHeader" TableSection="TableHeader" Visible="false" runat="server">
            <asp:TableCell Text="Title" Width="30%" />
            <asp:TableCell Text="Start" Width="30%" />
            <asp:TableCell Text="End" Width="30%" />
            <asp:TableCell Text="Actions" Width="10%" HorizontalAlign="Center" />
          </asp:TableRow>
        </asp:Table>
      </div>
    </div>

    <!-- Direct Report Items -->
    <asp:Panel ID="panelDirectReports" CssClass="column row" Visible="false" runat="server">
      <div class="table-scroll">
        <asp:Table ID="tableDirectReports" Caption="Direct Report Items" CssClass="hover" Width="100%" runat="server">
          <asp:TableRow ID="tableDirectReportsHeader" TableSection="TableHeader" runat="server">
            <asp:TableCell Text="Title" Width="22%" />
            <asp:TableCell Text="Start" Width="22%" />
            <asp:TableCell Text="End" Width="22%" />
            <asp:TableCell Text="Recur" Width="34%" />
          </asp:TableRow>
        </asp:Table>
      </div>
    </asp:Panel>

    <!-- Reveal Update -->
    <div id="update" class="tiny reveal" data-reveal="">
      <asp:Literal ID="updatePLead" runat="server"></asp:Literal>

      <table>
        <thead>
          <tr>
            <th style="text-align: left; font: inherit; width: 50%" colspan="2">
              <span data-tooltip="true" aria-haspopup="true" class="left" data-disable-hover="false" title="Your title should include a brief summary of what your event is about. Eg. Meeting, Holiday, EDO, Get-together, Birthday, Anniversary, etc.">
                <asp:Label ID="updateLblTitle" AssociatedControlID="updateTxtTitle" data-equalizer-watch="" Text="Title" runat="server"></asp:Label>
              </span>
              <asp:TextBox ID="updateTxtTitle" placeholder="EDO" runat="server"></asp:TextBox>
            </th>
            <th style="text-align: left; font: inherit; width: 50%" colspan="2">
              <span data-tooltip="true" aria-haspopup="true" class="right" data-disable-hover="false" title="Your division of Clearwater County.">
                <asp:Label ID="updateLblDepartment"
                  AssociatedControlID="updateDdlDepartment"
                  data-equalizer-watch=""
                  Text="Department"
                  runat="server"></asp:Label>
              </span>
              <asp:DropDownList ID="updateDdlDepartment" runat="server">
                <asp:ListItem Text="- SELECT -"></asp:ListItem>
                <asp:ListItem Text="Agricultural Services"></asp:ListItem>
                <asp:ListItem Text="CPS"></asp:ListItem>
                <asp:ListItem Text="Corporate Services"></asp:ListItem>
                <asp:ListItem Text="Planning"></asp:ListItem>
                <asp:ListItem Text="Public Works"></asp:ListItem>
              </asp:DropDownList>
            </th>
          </tr>

          <!-- All Day Event/Recurring Event -->
          <tr>
            <th style="text-align: left; font: inherit" colspan="2">
              <span data-tooltip="true" aria-haspopup="true" class="left" data-disable-hover="false"
                title="Removes the timestamp from your event item.">
                <asp:CheckBox ID="updateAllDayEvent"
                  AutoPostBack="true"
                  OnCheckedChanged="ClickAllDayEvent"
                  onclick="$('#updateClose').removeClass('fa-times').addClass('fa-cog fa-spin');"
                  Text="All Day Event"
                  runat="server" />
              </span>
            </th>

            <th style="text-align: left; font: inherit" colspan="2">
              <span data-tooltip="true" aria-haspopup="true" class="right" data-disable-hover="false" title="Create an event that occurs more than once, according to a pre-defined rule.">
                <asp:CheckBox ID="updateRecurringChb"
                  AutoPostBack="true"
                  onclick="$('#updateClose').removeClass('fa-times').addClass('fa-cog fa-spin');"
                  OnCheckedChanged="ClickRecurring"
                  Text="Recurring Event"
                  runat="server" />
              </span>
            </th>
          </tr>

          <tr id="row1" style="display: none">
            <th style="text-align: left; font: inherit; vertical-align: top" colspan="2">
              <span data-tooltip="true" aria-haspopup="true" class="left" data-disable-hover="false" title="Recur frequency is the event's repeating interval. The pattern section is change to accommodate your selection.">
                <asp:Label ID="updateRecurringLblFrequency"
                  AssociatedControlID="updateRecurringRbl"
                  data-equalizer-watch=""
                  Text="Frequency"
                  runat="server"></asp:Label>
                <asp:RadioButtonList ID="updateRecurringRbl"
                  AutoPostBack="true"
                  onclick="$('#updateClose').removeClass('fa-times').addClass('fa-cog fa-spin');"
                  OnSelectedIndexChanged="ClickRecurringRbl"
                  RepeatLayout="Flow"
                  runat="server">
                  <asp:ListItem Text="Daily" />
                  <asp:ListItem Text="Weekly" />
                  <asp:ListItem Text="Monthly" />
                  <asp:ListItem Text="Yearly" />
                </asp:RadioButtonList>
              </span>
            </th>
            <th style="text-align: left; font: inherit; vertical-align: top" colspan="2">
              <!-- Pattern -->
              <span data-tooltip="true" aria-haspopup="true" class="right" data-disable-hover="false" title="Recurrence pattern further defines the event's recur frequency. Variables X, Y, and Z are often used to match a control to it's place in the english sentence. For example, 'Every X Y of every Z month(s)' will read 'Every Third Friday of every 1 month(s)'.">
                <asp:Label ID="updateRecurringLblPattern"
                  AssociatedControlID="updateRecurringDailyRbl"
                  data-equalizer-watch=""
                  Text="Pattern"
                  runat="server"></asp:Label>

                <!-- Daily -->
                <asp:RadioButtonList ID="updateRecurringDailyRbl"
                  AutoPostBack="true"
                  onclick="$('#updateClose').removeClass('fa-times').addClass('fa-cog fa-spin');"
                  OnSelectedIndexChanged="ClickRecurringDaily"
                  RepeatLayout="Flow"
                  runat="server">
                  <asp:ListItem Text="Every X day(s)" />
                  <asp:ListItem Text="Every weekday" />
                </asp:RadioButtonList>
                <asp:TextBox ID="updateRecurringDailyTxtDays" placeholder="X" type="number" runat="server" />

                <!-- Weekly -->
                <asp:Label ID="updateRecurringWeeklyLblRecur"
                  AssociatedControlID="updateRecurringWeeklyTxtWeek"
                  data-equalizer-watch=""
                  Text="Every X week(s) on"
                  runat="server"></asp:Label>
                <asp:TextBox ID="updateRecurringWeeklyTxtWeek" placeholder="X" type="number" runat="server" />
                <asp:CheckBoxList ID="updateRecurringWeeklyChbDay" RepeatLayout="Flow" runat="server">
                  <asp:ListItem Text="Sunday" Value="su" />
                  <asp:ListItem Text="Monday" Value="mo" />
                  <asp:ListItem Text="Tuesday" Value="tu" />
                  <asp:ListItem Text="Wednesday" Value="we" />
                  <asp:ListItem Text="Thursday" Value="th" />
                  <asp:ListItem Text="Friday" Value="fr" />
                  <asp:ListItem Text="Saturday" Value="sa" />
                </asp:CheckBoxList>

                <!-- Monthly -->
                <asp:RadioButtonList ID="updateRecurringRblMonthly"
                  AutoPostBack="true"
                  onclick="$('#updateClose').removeClass('fa-times').addClass('fa-cog fa-spin');"
                  OnSelectedIndexChanged="ClickRecurringMonthly"
                  RepeatLayout="Flow"
                  runat="server">
                  <asp:ListItem Text="On the X day of every Y month(s)" />
                  <asp:ListItem Text="On the X Y of every Z month" />
                </asp:RadioButtonList>

                <asp:TextBox ID="updateRecurringMonthlyTxtDay" placeholder="X" type="number" runat="server" />
                <asp:TextBox ID="updateRecurringMonthlyTxtMonths" placeholder="Y" type="number" runat="server" />

                <asp:DropDownList ID="updateRecurringMonthlyDdlWeek" runat="server">
                  <asp:ListItem Text="X" />
                  <asp:ListItem Text="First" Value="first" />
                  <asp:ListItem Text="Second" Value="second" />
                  <asp:ListItem Text="Third" Value="third" />
                  <asp:ListItem Text="Fourth" Value="fourth" />
                  <asp:ListItem Text="Last" Value="last" />
                </asp:DropDownList>

                <asp:DropDownList ID="updateRecurringMonthlyDdlDay2" runat="server">
                  <asp:ListItem Text="Y" />
                  <asp:ListItem Text="Sunday" />
                  <asp:ListItem Text="Monday" />
                  <asp:ListItem Text="Tuesday" />
                  <asp:ListItem Text="Wednesday" />
                  <asp:ListItem Text="Thursday" />
                  <asp:ListItem Text="Friday" />
                  <asp:ListItem Text="Saturday" />
                  <asp:ListItem Text="Day" />
                  <asp:ListItem Text="Weekday" />
                  <asp:ListItem Text="Weekend Day" />
                </asp:DropDownList>
                <asp:TextBox ID="updateRecurringMonthlyTxtMonths2" placeholder="Z" type="number" runat="server" />

                <!-- Yearly -->
                <asp:RadioButtonList ID="updateRecurringYearlyRbl"
                  AutoPostBack="true"
                  onclick="$('#updateClose').removeClass('fa-times').addClass('fa-cog fa-spin');"
                  OnSelectedIndexChanged="ClickRecurringYealy"
                  RepeatLayout="Flow"
                  runat="server">
                  <asp:ListItem Text="On the X day of the Y month" />
                  <asp:ListItem Text="On the X Y of Z" />
                </asp:RadioButtonList>

                <asp:DropDownList ID="updateRecurringYearlyDdlMonth" runat="server">
                  <asp:ListItem Text="X" />
                  <asp:ListItem Text="January" />
                  <asp:ListItem Text="February" />
                  <asp:ListItem Text="March" />
                  <asp:ListItem Text="April" />
                  <asp:ListItem Text="May" />
                  <asp:ListItem Text="June" />
                  <asp:ListItem Text="July" />
                  <asp:ListItem Text="August" />
                  <asp:ListItem Text="September" />
                  <asp:ListItem Text="October" />
                  <asp:ListItem Text="November" />
                  <asp:ListItem Text="December" />
                </asp:DropDownList>
                <asp:TextBox ID="updateRecurringYearlyTxtDay" placeholder="Y" type="number" runat="server" />

                <asp:DropDownList ID="updateRecurringYearlyDdlWeek" runat="server">
                  <asp:ListItem Text="X" />
                  <asp:ListItem Text="First" Value="first" />
                  <asp:ListItem Text="Second" Value="second" />
                  <asp:ListItem Text="Third" Value="third" />
                  <asp:ListItem Text="Fourth" Value="fourth" />
                  <asp:ListItem Text="Last" Value="last" />
                </asp:DropDownList>

                <asp:DropDownList ID="updateRecurringYearlyDdlDay" runat="server">
                  <asp:ListItem Text="Y" />
                  <asp:ListItem Text="Sunday" />
                  <asp:ListItem Text="Monday" />
                  <asp:ListItem Text="Tuesday" />
                  <asp:ListItem Text="Wednesday" />
                  <asp:ListItem Text="Thursday" />
                  <asp:ListItem Text="Friday" />
                  <asp:ListItem Text="Saturday" />
                  <asp:ListItem Text="Day" />
                  <asp:ListItem Text="Weekday" />
                  <asp:ListItem Text="Weekend Day" />
                </asp:DropDownList>

                <asp:DropDownList ID="updateRecurringYearlyDdlMonth2" runat="server">
                  <asp:ListItem Text="Z" />
                  <asp:ListItem Text="January" />
                  <asp:ListItem Text="February" />
                  <asp:ListItem Text="March" />
                  <asp:ListItem Text="April" />
                  <asp:ListItem Text="May" />
                  <asp:ListItem Text="June" />
                  <asp:ListItem Text="July" />
                  <asp:ListItem Text="August" />
                  <asp:ListItem Text="September" />
                  <asp:ListItem Text="October" />
                  <asp:ListItem Text="November" />
                  <asp:ListItem Text="December" />
                </asp:DropDownList>

              </span>
            </th>
          </tr>

          <!-- Date Range -->
          <tr id="row2" style="display: none">
            <th style="text-align: left; font: inherit; vertical-align: top" colspan="2">
              <span data-tooltip="true" aria-haspopup="true" class="left" data-disable-hover="false" title="Specify the window your event will recur in.">
                <asp:Label ID="updateRecurringLblRange"
                  AssociatedControlID="updateRecurringRblRange"
                  data-equalizer-watch=""
                  Text="Date Range"
                  runat="server"></asp:Label>

                <asp:RadioButtonList ID="updateRecurringRblRange" AutoPostBack="true" OnSelectedIndexChanged="ClickRecurringRange" RepeatLayout="Flow" runat="server">
                  <asp:ListItem Text="No end date" />
                  <asp:ListItem Text="End after X occurrence(s)" />
                  <asp:ListItem Text="End by specific date" />
                </asp:RadioButtonList>
              </span>
            </th>
            <th style="text-align: left; font: inherit; vertical-align: top" colspan="2">
              <asp:Label ID="updateRecurringLblOccurrences" AssociatedControlID="updateRecurringTxtOccurrences" data-equalizer-watch="" Text="Occurrences" runat="server"></asp:Label>
              <asp:TextBox ID="updateRecurringTxtOccurrences" type="number" runat="server" />
              <asp:Label ID="updateRecurringLblEndDate" Text="End Date" AssociatedControlID="updateRecurringTxtEndDate" data-equalizer-watch="" runat="server"></asp:Label>
              <asp:TextBox ID="updateRecurringTxtEndDate" runat="server"></asp:TextBox>
            </th>
          </tr>

          <!-- Start/End -->
          <tr>
            <th style="text-align: left; font: inherit" colspan="1">
              <span data-tooltip="true" aria-haspopup="true" class="left" data-disable-hover="false" title="The date at which your event will begin.">
                <asp:Label ID="updateLblStartDate" Text="Start Date" AssociatedControlID="updateTxtStartDate" data-equalizer-watch="" runat="server" />
              </span>
              <asp:TextBox ID="updateTxtStartDate" runat="server" />
            </th>
            <th style="text-align: left; font: inherit" colspan="1">
              <span data-tooltip="true" aria-haspopup="true" class="left" data-disable-hover="false" title="The time at which your event will begin.">
                <asp:Label ID="updateLblStartTime" Text="Start Time" AssociatedControlID="updateTxtStartTime" data-equalizer-watch="" runat="server" />
              </span>
              <asp:TextBox ID="updateTxtStartTime" runat="server" />
            </th>
            <th style="text-align: left; font: inherit" colspan="1">
              <span data-tooltip="true" aria-haspopup="true" class="right" data-disable-hover="false" title="The date at which your event will end.">
                <asp:Label ID="updateLblEndDate" Text="End Date" AssociatedControlID="updateTxtEndDate" data-equalizer-watch="" runat="server" />
              </span>
              <asp:TextBox ID="updateTxtEndDate" runat="server" />
            </th>
            <th style="text-align: left; font: inherit" colspan="1">
              <span data-tooltip="true" aria-haspopup="true" class="right" data-disable-hover="false" title="The time at which your event will end.">
                <asp:Label ID="updateLblEndTime" Text="End Time" AssociatedControlID="updateTxtEndTime" data-equalizer-watch="" runat="server" />
              </span>
              <asp:TextBox ID="updateTxtEndTime" runat="server" />
            </th>
          </tr>

          <tr>
            <th style="text-align: left; font: inherit" colspan="4">
              <span data-tooltip="true" aria-haspopup="true" class="left" data-disable-hover="false" title="Details about your event.">
                <asp:Label ID="updateLblDescription" Text="Description" AssociatedControlID="updateTxtDescription" data-equalizer-watch="" runat="server"></asp:Label>
              </span>
              <asp:TextBox ID="updateTxtDescription" placeholder="Additional information" runat="server"></asp:TextBox>
            </th>
          </tr>
          <!-- Form Validation Messages -->
          <asp:Literal ID="formValidation" Visible="false" runat="server"></asp:Literal>
        </thead>
      </table>

      <div class="expanded button-group">
        <asp:LinkButton ID="updateAlert" CssClass="alert button" data-close="" OnClick="ClickClose" Text="Cancel" runat="server" />
        <asp:LinkButton ID="updateSuccess" CssClass="success button" Visible="false" OnClick="ClickUpdateSuccess" OnClientClick="$(this).html(&quot;<i class='fa fa-cog fa-spin fa-fw margin-bottom'></i><span class='sr-only'>Loading...</span>&quot;);" Text="Update" runat="server" />
        <asp:LinkButton ID="updateCreateSuccess" CssClass="success button" Visible="false" OnClick="ClickCreateSuccess" OnClientClick="$(this).html(&quot;<i class='fa fa-cog fa-spin fa-fw margin-bottom'></i><span class='sr-only'>Loading...</span>&quot;);" Text="Create" runat="server" />
      </div>
      <asp:LinkButton ID="updateClose" CssClass="close-button fa fa-times" OnClick="ClickClose" data-close="" aria-label="Close modal" runat="server" />
    </div>

    <!-- Reveal Delete -->
    <div id="delete" class="tiny reveal" data-reveal="">
      <asp:Literal ID="deletePLead" runat="server" />

      <table>
        <thead>
          <tr>
            <th style="text-align: left; font: inherit">
              <asp:Literal ID="deleteP" runat="server"></asp:Literal>
              <span data-tooltip="true" aria-haspopup="true" class="left" data-disable-hover="false" title="A cause, explanation, or justification for removing this event.">
                <asp:Label ID="deleteLblWhy" Text="Why?" AssociatedControlID="deleteWhy" runat="server"></asp:Label>
              </span>
              <asp:TextBox ID="deleteWhy" runat="server" />
            </th>
          </tr>
        </thead>
      </table>

      <div class="expanded button-group">
        <asp:LinkButton ID="deleteAlert" CssClass="alert button" data-close="" OnClick="ClickClose" Text="No" runat="server" />
        <asp:LinkButton ID="deleteSuccess" CssClass="success button" OnClick="ClickDeleteSuccess" OnClientClick="$(this).html(&quot;<i class='fa fa-cog fa-spin fa-fw margin-bottom'></i><span class='sr-only'>Loading...</span>&quot;);" Text="Yes" runat="server" />
        <asp:LinkButton ID="revealApproveSuccess" CssClass="success button" OnClick="ClickApproveSuccess" OnClientClick="$(this).html(&quot;<i class='fa fa-cog fa-spin fa-fw margin-bottom'></i><span class='sr-only'>Loading...</span>&quot;);" Text="Yes" runat="server" />
        <asp:LinkButton ID="revealRejectSuccess" CssClass="success button" OnClick="ClickRejectSuccess" OnClientClick="$(this).html(&quot;<i class='fa fa-cog fa-spin fa-fw margin-bottom'></i><span class='sr-only'>Loading...</span>&quot;);" Text="Yes" runat="server" />
      </div>
      <asp:LinkButton CssClass="close-button fa fa-times" OnClick="ClickClose" data-close="" aria-label="Close modal" aria-hidden="" runat="server" />
    </div>

    <!-- JavaScript -->
    <script src="JavaScript/jquery-2.2.2.min.js"></script>
    <script src="JavaScript/foundation.min.js"></script>
    <script src="JavaScript/foundation-datepicker.min.js"></script>
    <script type="text/javascript">
      $(document).foundation();
      $(function () {
        $('#updateTxtStartDate, #updateTxtEndDate, #updateRecurringTxtEndDate').fdatepicker({
          format: 'yyyy-mm-dd',
          startView: "month"
        });
        $('#updateTxtStartTime, #updateTxtEndTime').fdatepicker({
          format: 'hh:ii:ss',
          startView: "day",
          pickTime: true
        });
      });

      function ToggleVisibility() {
        var element = document.getElementByID('updateAllDayEvent');

        if (element.style.display = 'none') {
          element.style.display = 'inherit';
        }
        else {
          element.style.display = 'none';
        }
      }
    </script>
  </form>
</body>
</html>
