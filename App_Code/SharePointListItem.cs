﻿using Microsoft.SharePoint.Client;
using System;
using System.Globalization;
using System.IO;
using System.Xml;

/// <summary>
/// Summary description for SharePointListItem
/// </summary>
public class SharePointListItem
{
  private string myListTitle;
  private ListItem myListItem;
  private SharePointUser myUser;

  public SharePointListItem(string listTitle, ListItem listItem, SharePointUser user)
  {
    myListTitle = listTitle;
    myListItem = listItem;
    myUser = user;
  }

  public string[] DecodeDateTime(object dateTimeString)
  {
    DateTime dateTime;
    if ((bool)FAllDayEvent) dateTime = Convert.ToDateTime(dateTimeString);
    else dateTime = Convert.ToDateTime(dateTimeString).ToLocalTime();

    string[] output = new string[2];
    output[0] = dateTime.ToString("yyyy-MM-dd");
    output[1] = dateTime.ToString("HH:mm:ss");
    return output;
  }

  public string DecodeRecurrenceData()
  {
    // Convert recurrence data to human language
    string output = "";
    if ((bool)FRecurrence)
    {
      if (Daily != null)
      {
        if (DailyDayFrequency != null)
          output += "Every " + DailyDayFrequency + " day(s)";
        else if (DailyWeekday != null)
          output += "Every weekday";
      }

      else if (Weekly != null)
      {
        output += "Every " + WeeklyWeekFrequency + " week(s) on ";
        if (WeeklySu != null) output += " Sunday";
        if (WeeklyMo != null) output += " Monday";
        if (WeeklyTu != null) output += " Tuesday";
        if (WeeklyWe != null) output += " Wednesday";
        if (WeeklyTh != null) output += " Thursday";
        if (WeeklyFr != null) output += " Friday";
        if (WeeklySa != null) output += " Saturday";
      }

      else if (Monthly != null)
        output += "On the " + AddOrdinal(Convert.ToInt32(MonthlyDay)) +
          " day of every " + MonthlyMonthFrequency + " month(s)";

      else if (MonthlyByDay != null)
      {
        output += "On the " + MonthlyByDayWeekdayOfMonth + " ";
        if (MonthlyByDaySu != null) output += " Sunday";
        else if (MonthlyByDayMo != null) output += " Monday";
        else if (MonthlyByDayTu != null) output += " Tuesday";
        else if (MonthlyByDayWe != null) output += " Wednesday";
        else if (MonthlyByDayTh != null) output += " Thursday";
        else if (MonthlyByDayFr != null) output += " Friday";
        else if (MonthlyByDaySa != null) output += " Saturday";
        else if (MonthlyByDayDay != null) output += " day";
        else if (MonthlyByDayWeekday != null) output += " weekday";
        else if (MonthlyByDayWeekend_day != null) output += " weekend day";
        output += " of every " + MonthlyByDayMonthFrequency + " month(s)";
      }

      else if (Yearly != null)
        output += "On the " +
          AddOrdinal(Convert.ToInt32(YearlyDay)) + " day of the " +
          AddOrdinal(Convert.ToInt32(YearlyMonth)) + " month";

      else if (YearlyByDay != null)
      {
        output += "On the " + YearlyByDayWeekdayOfMonth;
        if (YearlyByDaySu != null) output += " Sunday";
        else if (YearlyByDayMo != null) output += " Monday";
        else if (YearlyByDayTu != null) output += " Tuesday";
        else if (YearlyByDayWe != null) output += " Wednesday";
        else if (YearlyByDayTh != null) output += " Thursday";
        else if (YearlyByDayFr != null) output += " Friday";
        else if (YearlyByDaySa != null) output += " Saturday";
        else if (YearlyByDayDay != null) output += " day";
        else if (YearlyByDayWeekday != null) output += " weekday";
        else if (YearlyByDayWeekend_day != null) output += " weekend day";
        output += " of " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToInt32(YearlyByDayMonth));
      }

      if (RepeatForever != null)
        output += " forever";

      else if (RepeatInstances != null)
        output += " for " + RepeatInstances + " instances";

      else if (WindowEnd != null)
        output += " until " + Convert.ToDateTime(WindowEnd).ToUniversalTime().ToString("MMMM d, yyyy h:mm tt");
    }
    else output = "None";
    return output;
  }

  public void UnwrapRecurringXML()
  {
    // http://thehightechheels.blogspot.ca/2012/12/sharepoint-evenet-recurrencedata-xml.html
    FirstDayOfWeek = null;

    Daily = null;
    DailyDayFrequency = null;
    DailyWeekday = null;

    Weekly = null;
    WeeklyWeekFrequency = null;
    WeeklySu = null;
    WeeklyMo = null;
    WeeklyTu = null;
    WeeklyWe = null;
    WeeklyTh = null;
    WeeklyFr = null;
    WeeklySa = null;

    Monthly = null;
    MonthlyDay = null;
    MonthlyMonthFrequency = null;

    MonthlyByDay = null;
    MonthlyByDayMonthFrequency = null;
    MonthlyByDaySu = null;
    MonthlyByDayMo = null;
    MonthlyByDayTu = null;
    MonthlyByDayWe = null;
    MonthlyByDayTh = null;
    MonthlyByDayFr = null;
    MonthlyByDaySa = null;
    MonthlyByDayDay = null;
    MonthlyByDayWeekday = null;
    MonthlyByDayWeekend_day = null;
    MonthlyByDayWeekdayOfMonth = null;

    Yearly = null;
    YearlyYearFrequency = null;
    YearlyMonth = null;
    YearlyDay = null;

    YearlyByDay = null;
    YearlyByDayYearFrequency = null;
    YearlyByDayMonth = null;
    YearlyByDaySu = null;
    YearlyByDayMo = null;
    YearlyByDayTu = null;
    YearlyByDayWe = null;
    YearlyByDayTh = null;
    YearlyByDayFr = null;
    YearlyByDaySa = null;
    YearlyByDayDay = null;
    YearlyByDayWeekday = null;
    YearlyByDayWeekend_day = null;
    YearlyByDayWeekdayOfMonth = null;

    RepeatInstances = null;
    WindowEnd = null;
    RepeatForever = null;

    // Create an XML reader for this file.
    using (XmlReader reader = XmlReader.Create((new StringReader((string)RecurrenceData))))
    {
      while (reader.Read())
      {
        // Only detect start elements.
        if (reader.IsStartElement())
        {
          // Get element name and switch on it.
          switch (reader.Name)
          {
            case "firstDayOfWeek":
              if (reader.Read()) FirstDayOfWeek = reader.Value;
              break;

            case "daily":
              Daily = true;
              if (reader["dayFrequency"] != null) DailyDayFrequency = reader["dayFrequency"];
              else if (reader["weekday"] != null) DailyWeekday = reader["weekday"];
              break;

            case "weekly":
              Weekly = true;
              if (reader["weekFrequency"] != null) WeeklyWeekFrequency = reader["weekFrequency"];
              if (reader["su"] != null) WeeklySu = reader["su"];
              if (reader["mo"] != null) WeeklyMo = reader["mo"];
              if (reader["tu"] != null) WeeklyTu = reader["tu"];
              if (reader["we"] != null) WeeklyWe = reader["we"];
              if (reader["th"] != null) WeeklyTh = reader["th"];
              if (reader["fr"] != null) WeeklyFr = reader["fr"];
              if (reader["sa"] != null) WeeklySa = reader["sa"];
              break;

            case "monthly":
              Monthly = true;
              if (reader["day"] != null) MonthlyDay = reader["day"];
              if (reader["monthFrequency"] != null) MonthlyMonthFrequency = reader["monthFrequency"];
              break;

            case "monthlyByDay":
              MonthlyByDay = true;
              if (reader["monthFrequency"] != null) MonthlyByDayMonthFrequency = reader["monthFrequency"];
              if (reader["su"] != null) MonthlyByDaySu = reader["su"];
              if (reader["mo"] != null) MonthlyByDayMo = reader["mo"];
              if (reader["tu"] != null) MonthlyByDayTu = reader["tu"];
              if (reader["we"] != null) MonthlyByDayWe = reader["we"];
              if (reader["th"] != null) MonthlyByDayTh = reader["th"];
              if (reader["fr"] != null) MonthlyByDayFr = reader["fr"];
              if (reader["sa"] != null) MonthlyByDaySa = reader["sa"];
              if (reader["day"] != null) MonthlyByDayDay = reader["day"];
              if (reader["weekday"] != null) MonthlyByDayWeekday = reader["weekday"];
              if (reader["weekend_day"] != null) MonthlyByDayWeekend_day = reader["weekend_day"];
              if (reader["weekdayOfMonth"] != null) MonthlyByDayWeekdayOfMonth = reader["weekdayOfMonth"];
              break;

            case "yearly":
              Yearly = true;
              if (reader["yearFrequency"] != null) YearlyYearFrequency = reader["yearFrequency"];
              if (reader["month"] != null) YearlyMonth = reader["month"];
              if (reader["day"] != null) YearlyDay = reader["day"];
              break;

            case "yearlyByDay":
              YearlyByDay = true;
              if (reader["yearFrequency"] != null) YearlyByDayYearFrequency = reader["yearFrequency"];
              if (reader["month"] != null) YearlyByDayMonth = reader["month"];
              if (reader["su"] != null) YearlyByDaySu = reader["su"];
              if (reader["mo"] != null) YearlyByDayMo = reader["mo"];
              if (reader["tu"] != null) YearlyByDayTu = reader["tu"];
              if (reader["we"] != null) YearlyByDayWe = reader["we"];
              if (reader["th"] != null) YearlyByDayTh = reader["th"];
              if (reader["fr"] != null) YearlyByDayFr = reader["fr"];
              if (reader["sa"] != null) YearlyByDaySa = reader["sa"];
              if (reader["day"] != null) YearlyByDayDay = reader["day"];
              if (reader["weekday"] != null) YearlyByDayWeekday = reader["weekday"];
              if (reader["weekend_day"] != null) YearlyByDayWeekend_day = reader["weekend_day"];
              if (reader["weekdayOfMonth"] != null) YearlyByDayWeekdayOfMonth = reader["weekdayOfMonth"];
              break;

            case "repeatInstances":
              if (reader.Read()) RepeatInstances = reader.Value;
              break;

            case "windowEnd":
              if (reader.Read()) WindowEnd = reader.Value;
              break;

            case "repeatForever":
              if (reader.Read()) RepeatForever = reader.Value;
              break;

            default: break;
          }
        }
      }
    }
  }

  private string AddOrdinal(int num)
  {
    switch (num)
    {
      case 1: return num + "<sup>st</sup>";
      case 2: return num + "<sup>nd</sup>";
      case 3: return num + "<sup>rd</sup>";
      default: return num + "<sup>th</sup>";
    }
  }

  public string ListTitle
  {
    get { return myListTitle; }
    set { myListTitle = value; }
  }
  public object Title
  {
    get { return myListItem["Title"]; }
    set { myListItem["Title"] = value; }
  }
  public object Person
  {
    get { return myListItem["Person"]; }
    set { myListItem["Person"] = value; }
  }
  public DateTime EventDate
  {
    get
    {
      DateTime dt;
      if ((bool)FAllDayEvent) dt = Convert.ToDateTime(myListItem["EventDate"]);
      else dt = Convert.ToDateTime(myListItem["EventDate"]).ToLocalTime();
      return dt;
    }
    set { myListItem["EventDate"] = value; }
  }
  public DateTime EndDate
  {
    get
    {
      DateTime dt;
      if ((bool)FAllDayEvent) dt = Convert.ToDateTime(myListItem["EndDate"]);
      else dt = Convert.ToDateTime(myListItem["EndDate"]).ToLocalTime();
      return dt;
    }
    set { myListItem["EndDate"] = value; }
  }
  public object Description
  {
    get { return myListItem["Description"]; }
    set { myListItem["Description"] = value; }
  }
  public object Department
  {
    get { return myListItem["Department"]; }
    set { myListItem["Department"] = value; }
  }
  public bool FAllDayEvent
  {
    get { return (bool)myListItem["fAllDayEvent"]; }
    set { myListItem["fAllDayEvent"] = value; }
  }
  public bool FRecurrence
  {
    get { return (bool)myListItem["fRecurrence"]; }
    set { myListItem["fRecurrence"] = value; }
  }
  public object RecurrenceData
  {
    get { return myListItem["RecurrenceData"]; }
    set { myListItem["RecurrenceData"] = value; }
  }
  public object EventType
  {
    get { return myListItem["EventType"]; }
    set { myListItem["EventType"] = value; }
  }
  public object ID
  {
    get { return myListItem["ID"]; }
    set { myListItem["ID"] = value; }
  }

  public object FirstDayOfWeek
  {
    get { return myListItem["firstDayOfWeek"]; }
    set { myListItem["firstDayOfWeek"] = value; }
  }
  public object Daily
  {
    get { return myListItem["daily"]; }
    set { myListItem["daily"] = value; }
  }
  public object DailyDayFrequency
  {
    get { return myListItem["dailyDayFrequency"]; }
    set { myListItem["dailyDayFrequency"] = value; }
  }
  public object DailyWeekday
  {
    get { return myListItem["dailyWeekday"]; }
    set { myListItem["dailyWeekday"] = value; }
  }
  public object Weekly
  {
    get { return myListItem["weekly"]; }
    set { myListItem["weekly"] = value; }
  }
  public object WeeklyWeekFrequency
  {
    get { return myListItem["weeklyWeekFrequency"]; }
    set { myListItem["weeklyWeekFrequency"] = value; }
  }
  public object WeeklySu
  {
    get { return myListItem["weeklySu"]; }
    set { myListItem["weeklySu"] = value; }
  }
  public object WeeklyMo
  {
    get { return myListItem["weeklyMo"]; }
    set { myListItem["weeklyMo"] = value; }
  }
  public object WeeklyTu
  {
    get { return myListItem["weeklyTu"]; }
    set { myListItem["weeklyTu"] = value; }
  }
  public object WeeklyWe
  {
    get { return myListItem["weeklyWe"]; }
    set { myListItem["weeklyWe"] = value; }
  }
  public object WeeklyTh
  {
    get { return myListItem["weeklyTh"]; }
    set { myListItem["weeklyTh"] = value; }
  }
  public object WeeklyFr
  {
    get { return myListItem["weeklyFr"]; }
    set { myListItem["weeklyFr"] = value; }
  }
  public object WeeklySa
  {
    get { return myListItem["weeklySa"]; }
    set { myListItem["weeklySa"] = value; }
  }
  public object Monthly
  {
    get { return myListItem["monthly"]; }
    set { myListItem["monthly"] = value; }
  }
  public object MonthlyDay
  {
    get { return myListItem["monthlyDay"]; }
    set { myListItem["monthlyDay"] = value; }
  }
  public object MonthlyMonthFrequency
  {
    get { return myListItem["monthlyMonthFrequency"]; }
    set { myListItem["monthlyMonthFrequency"] = value; }
  }
  public object MonthlyByDay
  {
    get { return myListItem["monthlyByDay"]; }
    set { myListItem["monthlyByDay"] = value; }
  }
  public object MonthlyByDayWeekdayOfMonth
  {
    get { return myListItem["monthlyByDayWeekdayOfMonth"]; }
    set { myListItem["monthlyByDayWeekdayOfMonth"] = value; }
  }
  public object MonthlyByDaySu
  {
    get { return myListItem["monthlyByDaySu"]; }
    set { myListItem["monthlyByDaySu"] = value; }
  }
  public object MonthlyByDayMo
  {
    get { return myListItem["monthlyByDayMo"]; }
    set { myListItem["monthlyByDayMo"] = value; }
  }
  public object MonthlyByDayTu
  {
    get { return myListItem["monthlyByDayTu"]; }
    set { myListItem["monthlyByDayTu"] = value; }
  }
  public object MonthlyByDayWe
  {
    get { return myListItem["monthlyByDayWe"]; }
    set { myListItem["monthlyByDayWe"] = value; }
  }
  public object MonthlyByDayTh
  {
    get { return myListItem["monthlyByDayTh"]; }
    set { myListItem["monthlyByDayTh"] = value; }
  }
  public object MonthlyByDayFr
  {
    get { return myListItem["monthlyByDayFr"]; }
    set { myListItem["monthlyByDayFr"] = value; }
  }
  public object MonthlyByDaySa
  {
    get { return myListItem["monthlyByDaySa"]; }
    set { myListItem["monthlyByDaySa"] = value; }
  }
  public object MonthlyByDayDay
  {
    get { return myListItem["monthlyByDayDay"]; }
    set { myListItem["monthlyByDayDay"] = value; }
  }
  public object MonthlyByDayWeekday
  {
    get { return myListItem["monthlyByDayWeekday"]; }
    set { myListItem["monthlyByDayWeekday"] = value; }
  }
  public object MonthlyByDayWeekend_day
  {
    get { return myListItem["monthlyByDayWeekend_day"]; }
    set { myListItem["monthlyByDayWeekend_day"] = value; }
  }
  public object MonthlyByDayMonthFrequency
  {
    get { return myListItem["monthlyByDayMonthFrequency"]; }
    set { myListItem["monthlyByDayMonthFrequency"] = value; }
  }
  public object Yearly
  {
    get { return myListItem["yearly"]; }
    set { myListItem["yearly"] = value; }
  }
  public object YearlyYearFrequency
  {
    get { return myListItem["yearlyYearFrequency"]; }
    set { myListItem["yearlyYearFrequency"] = value; }
  }
  public object YearlyMonth
  {
    get { return myListItem["yearlyMonth"]; }
    set { myListItem["yearlyMonth"] = value; }
  }
  public object YearlyDay
  {
    get { return myListItem["yearlyDay"]; }
    set { myListItem["yearlyDay"] = value; }
  }
  public object YearlyByDay
  {
    get { return myListItem["yearlyByDay"]; }
    set { myListItem["yearlyByDay"] = value; }
  }
  public object YearlyByDayYearFrequency
  {
    get { return myListItem["yearlyByDayYearFrequency"]; }
    set { myListItem["yearlyByDayYearFrequency"] = value; }
  }
  public object YearlyByDayWeekdayOfMonth
  {
    get { return myListItem["yearlyByDayWeekdayOfMonth"]; }
    set { myListItem["yearlyByDayWeekdayOfMonth"] = value; }
  }
  public object YearlyByDaySu
  {
    get { return myListItem["yearlyByDaySu"]; }
    set { myListItem["yearlyByDaySu"] = value; }
  }
  public object YearlyByDayMo
  {
    get { return myListItem["yearlyByDayMo"]; }
    set { myListItem["yearlyByDayMo"] = value; }
  }
  public object YearlyByDayTu
  {
    get { return myListItem["yearlyByDayTu"]; }
    set { myListItem["yearlyByDayTu"] = value; }
  }
  public object YearlyByDayWe
  {
    get { return myListItem["yearlyByDayWe"]; }
    set { myListItem["yearlyByDayWe"] = value; }
  }
  public object YearlyByDayTh
  {
    get { return myListItem["yearlyByDayTh"]; }
    set { myListItem["yearlyByDayTh"] = value; }
  }
  public object YearlyByDayFr
  {
    get { return myListItem["yearlyByDayFr"]; }
    set { myListItem["yearlyByDayFr"] = value; }
  }
  public object YearlyByDaySa
  {
    get { return myListItem["yearlyByDaySa"]; }
    set { myListItem["yearlyByDaySa"] = value; }
  }
  public object YearlyByDayDay
  {
    get { return myListItem["yearlyByDayDay"]; }
    set { myListItem["yearlyByDayDay"] = value; }
  }
  public object YearlyByDayWeekday
  {
    get { return myListItem["yearlyByDayWeekday"]; }
    set { myListItem["yearlyByDayWeekday"] = value; }
  }
  public object YearlyByDayWeekend_day
  {
    get { return myListItem["yearlyByDayWeekend_day"]; }
    set { myListItem["yearlyByDayWeekend_day"] = value; }
  }
  public object YearlyByDayMonth
  {
    get { return myListItem["yearlyByDayMonth"]; }
    set { myListItem["yearlyByDayMonth"] = value; }
  }
  public object RepeatForever
  {
    get { return myListItem["repeatForever"]; }
    set { myListItem["repeatForever"] = value; }
  }
  public object RepeatInstances
  {
    get { return myListItem["repeatInstances"]; }
    set { myListItem["repeatInstances"] = value; }
  }
  public object WindowEnd
  {
    get { return myListItem["windowEnd"]; }
    set { myListItem["windowEnd"] = value; }
  }
}