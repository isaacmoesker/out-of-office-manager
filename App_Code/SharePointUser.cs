﻿using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Web;
using System.DirectoryServices;
using System.Collections.ObjectModel;
using System.Net.Mail;
using System.Xml;
using System.IO;

/// <summary>
/// Summary description for SharePointUser
/// </summary>
public class SharePointUser
{
  private string mySAMAccountName;
  private string myDomain;
  private string myWebFullUrl;
  private string manager; // sAMAccountName
  private string title; // Full name: "Isaac Moesker"
  private int id;
  private Collection<string> directReports;

  private Dictionary<object, SharePointListItem> myItems;
  private Dictionary<object, SharePointListItem> pendingItems;
  private Dictionary<object, SharePointListItem> actionItems;

  private SmtpClient smtpClient;
  private MailMessage mailMessage;

  public SharePointUser(string sAMAccountName, string domain = "CLEARWATERCOUNT", string webFullUrl = "http://stream.clearwatercounty.ca/")
  {
    mySAMAccountName = sAMAccountName;
    myDomain = domain;
    myWebFullUrl = webFullUrl;

    myItems = new Dictionary<object, SharePointListItem>();
    pendingItems = new Dictionary<object, SharePointListItem>();
    actionItems = new Dictionary<object, SharePointListItem>();

    using (ClientContext context = new ClientContext(myWebFullUrl))
    {
      User user = context.Web.EnsureUser(mySAMAccountName);
      context.Load(user);
      context.ExecuteQuery();
      Id = user.Id;
      Title = user.Title;
    }
  }

  public void RetrieveMyItems()
  {
    // Starting with ClientContext, the constructor requires a URL to the 
    // server running SharePoint. 
    using (ClientContext context = new ClientContext(myWebFullUrl))
    {
      // Assume the web has a list named "Announcements".
      List list = context.Web.Lists.GetByTitle("Out of Office");

      // so that it grabs all list items, regardless of the folder they are in. 
      CamlQuery query = new CamlQuery();
      query.ViewXml = "<View><Query><Where><And><Eq><FieldRef Name='Person' LookupId='True' /><Value Type='User'>" + Id + "</Value></Eq>" +
        "<Geq><FieldRef Name='EndDate' /><Value IncludeTimeValue='True' Type='DateTime'><Today /></Value></Geq></And></Where></Query></View>";
      ListItemCollection lic = list.GetItems(query);

      // Retrieve all items in the ListItemCollection from List.GetItems(Query). 
      context.Load(lic);
      context.ExecuteQuery();

      foreach (ListItem listItem in lic)
      {
        SharePointListItem spListItem = new SharePointListItem("Out of Office", listItem, this);
        myItems.Add(listItem["ID"], spListItem);
        Id = ((FieldUserValue)listItem["Person"]).LookupId;
      }
    }

    foreach (var item in myItems.Values)
      if ((bool)item.FRecurrence)
        item.UnwrapRecurringXML();
  }

  public void RetrievePendingItems()
  {
    // Starting with ClientContext, the constructor requires a URL to the 
    // server running SharePoint. 
    using (ClientContext context = new ClientContext(myWebFullUrl))
    {
      // Assume the web has a list named "Announcements".
      List list = context.Web.Lists.GetByTitle("Out of Office Action Items");

      // so that it grabs all list items, regardless of the folder they are in. 
      CamlQuery query = new CamlQuery();
      query.ViewXml = "<View><Query><Where><And><Eq><FieldRef Name='Person' LookupId='True' /><Value Type='User'>" + Id + "</Value></Eq>" +
        "<Geq><FieldRef Name='EndDate' /><Value IncludeTimeValue='True' Type='DateTime'><Today /></Value></Geq></And></Where></Query></View>";
      ListItemCollection lic = list.GetItems(query);

      // Retrieve all items in the ListItemCollection from List.GetItems(Query). 
      context.Load(lic);
      context.ExecuteQuery();

      foreach (ListItem listItem in lic)
      {
        SharePointListItem spListItem = new SharePointListItem("Out of Office Action Items", listItem, this);
        pendingItems.Add(listItem["ID"], spListItem);
      }
    }

    foreach (var item in pendingItems.Values)
      if ((bool)item.FRecurrence)
        item.UnwrapRecurringXML();

  }

  public void RetrieveActionItems()
  {
    // Starting with ClientContext, the constructor requires a URL to the 
    // server running SharePoint.
    using (ClientContext context = new ClientContext(myWebFullUrl))
    {
      List<string> camls = new List<string>();
      foreach (string sAMAccountName in directReports)
      {
        // Get SharePoint User ID
        User user = context.Web.EnsureUser(sAMAccountName);
        context.Load(user);
        context.ExecuteQuery();
        camls.Add("<Eq><FieldRef Name='Person' LookupId='True' /><Value Type='User'>" + user.Id + "</Value></Eq>");
      }

      // so that it grabs all list items, regardless of the folder they are in. 
      CamlQuery query = new CamlQuery();

      if (directReports.Count > 2)
      {
        string queryString = "<Or>" + camls[0] + camls[1] + "</Or>";
        for (int i = 2; i < camls.Count; i++)
          queryString = "<Or>" + queryString + camls[i] + "</Or>";
        query.ViewXml = "<View><Query><Where>" + queryString + "</Where></Query></View>";
      }
      else if (directReports.Count > 1)
        query.ViewXml = "<View><Query><Where><Or>" + camls[0] + camls[1] + "</Or></Where></Query></View>";
      else if (directReports.Count == 1)
        query.ViewXml = "<View><Query><Where>" + camls[0] + "</Where></Query></View>";
      else
        query.ViewXml = "<View><Query><Where><Eq><FieldRef Name='Person' LookupId='True' /><Value Type='User'>0</Value></Eq></Where></Query></View>";

      List list = context.Web.Lists.GetByTitle("Out of Office Action Items");
      ListItemCollection lic = list.GetItems(query);

      // Retrieve all items in the ListItemCollection from List.GetItems(Query). 
      context.Load(lic);
      context.ExecuteQuery();

      foreach (ListItem listItem in lic)
      {
        SharePointListItem spListItem = new SharePointListItem("Out of Office", listItem, this);
        actionItems.Add(listItem["ID"], spListItem);
      }
    }

    foreach (var item in actionItems.Values)
      if ((bool)item.FRecurrence)
        item.UnwrapRecurringXML();
  }

  public SharePointListItem CreateListItem(
    string listTitle,
    object title,
    object person,
    object eventDate,
    object endDate,
    object description,
    object department,
    object fAllDayEvent,
    object fRecurrence,
    object recurrenceData,
    object eventType)
  {
    SharePointListItem listItem;
    // Starting with ClientContext, the constructor requires a URL to the 
    // server running SharePoint.
    using (ClientContext context = new ClientContext(myWebFullUrl))
    {
      /*User user = context.Web.EnsureUser(sAMAccountName);
      context.Load(user);
      context.ExecuteQuery();*/

      // Assume that the web has a list named "Announcements". 
      List list = context.Web.Lists.GetByTitle(listTitle);

      // We are just creating a regular list item, so we don't need to 
      // set any properties. If we wanted to create a new folder, for 
      // example, we would have to set properties such as 
      // UnderlyingObjectType to FileSystemObjectType.Folder. 
      ListItemCreationInformation itemCreateInfo = new ListItemCreationInformation();
      ListItem newItem = list.AddItem(itemCreateInfo);

      newItem["Title"] = title;
      newItem["Person"] = person;
      newItem["EventDate"] = eventDate;
      newItem["EndDate"] = endDate;
      newItem["Description"] = description;
      newItem["Department"] = department;
      newItem["fAllDayEvent"] = fAllDayEvent;
      newItem["fRecurrence"] = fRecurrence;
      newItem["RecurrenceData"] = recurrenceData;
      newItem["EventType"] = eventType;
      newItem["UID"] = Guid.NewGuid();
      newItem["TimeZone"] = 0;
      newItem["XMLTZone"] = "<timeZoneRule><standardBias>0</standardBias><additionalDaylightBias>0</additionalDaylightBias></timeZoneRule>";

      newItem.Update();
      context.ExecuteQuery();

      listItem = new SharePointListItem("Out of Office", newItem, this);
      listItem.ID = newItem.Id;
      MyItems.Add(newItem.Id, listItem);
    }
    return listItem;
  }

  public void UpdateListItem(SharePointListItem item, string name)
  {
    if (item.ListTitle == "Out of Office")
    {
      // Create the updated item in Action Items
      CreateListItem(
        "Out of Office Action Items",
        item.Title,
        item.Person,
        item.EventDate,
        item.EndDate,
        item.Description,
        item.Department,
        item.FAllDayEvent,
        item.FRecurrence,
        item.RecurrenceData,
        item.EventType);

      // Delete the old item in Out of Office
      DeleteListItem(item);
    }
    else
    {
      // Starting with ClientContext, the constructor requires a URL to the 
      // server running SharePoint. 
      using (ClientContext context = new ClientContext(myWebFullUrl))
      {
        // Assume that the web has a list named "Announcements". 
        List spList = context.Web.Lists.GetByTitle("Out of Office Action Items");

        // Assume there is a list item with ID=1.
        ListItem listItem = spList.GetItemById((int)item.ID);

        // Write a new value to the Body field of the Announcement item.
        listItem["Title"] = item.Title;
        listItem["Person"] = item.Person;
        listItem["EventDate"] = item.EventDate;
        listItem["EndDate"] = item.EndDate;
        listItem["Description"] = item.Description;
        listItem["Department"] = item.Department;
        listItem["fAllDayEvent"] = item.FAllDayEvent;
        listItem["fRecurrence"] = item.FRecurrence;
        listItem["RecurrenceData"] = item.RecurrenceData;
        listItem["EventType"] = item.EventType;

        listItem.Update();

        context.ExecuteQuery();
      }
    }
  }

  public void DeleteListItem(SharePointListItem item)
  {
    // Starting with ClientContext, the constructor requires a URL to the 
    // server running SharePoint. 
    using (ClientContext context = new ClientContext(myWebFullUrl))
    {
      // Assume that the web has a list named "Announcements". 
      List spList = context.Web.Lists.GetByTitle(item.ListTitle);

      // Assume that there is a list item with ID=2. 
      ListItem listItem = spList.GetItemById((int)item.ID);
      listItem.DeleteObject();
      MyItems.Remove(item.ID);
      item = null;

      context.ExecuteQuery();
    }
  }

  public void ApproveListItem(int id)
  {
    CreateListItem(
      "Out of Office",
      actionItems[id].Title,
      actionItems[id].Person,
      actionItems[id].EventDate,
      actionItems[id].EndDate,
      actionItems[id].Description,
      actionItems[id].Department,
      actionItems[id].FAllDayEvent,
      actionItems[id].FRecurrence,
      actionItems[id].RecurrenceData,
      actionItems[id].EventType
    );

    // Starting with ClientContext, the constructor requires a URL to the 
    // server running SharePoint. 
    using (ClientContext context = new ClientContext(myWebFullUrl))
    {
      // Assume that there is a list item with ID=2.
      List list = context.Web.Lists.GetByTitle("Out of Office Action Items");
      ListItem listItem = list.GetItemById((int)actionItems[id].ID);
      listItem.DeleteObject();
      context.ExecuteQuery();
    }
  }

  public void RejectListItem(int id)
  {
    // Starting with ClientContext, the constructor requires a URL to the 
    // server running SharePoint. 
    using (ClientContext context = new ClientContext(myWebFullUrl))
    {
      // Assume that there is a list item with ID=2.
      List list = context.Web.Lists.GetByTitle("Out of Office Action Items");
      ListItem listItem = list.GetItemById((int)actionItems[id].ID);
      listItem.DeleteObject();

      context.ExecuteQuery();
    }
  }

  public void RetrieveOrganizationStructure(string ldapBase = "LDAP://DC=CLEARWATERCOUNTY,DC=CA")
  {
    Collection<string> result = new Collection<string>();

    using (DirectoryEntry de = new DirectoryEntry(ldapBase))
    {
      string userDName;
      string managerDName = null;
      bool getManager = true;
      int uac;

      using (DirectorySearcher ds = new DirectorySearcher(de))
      {
        ds.PropertiesToLoad.Add("distinguishedName");
        ds.PropertiesToLoad.Add("manager");
        ds.Filter = "(&(objectCategory=user)(sAMAccountName=" + mySAMAccountName + "))";

        SearchResult sr = ds.FindOne();
        userDName = (string)sr.Properties["distinguishedName"][0];

        try { managerDName = (string)sr.Properties["manager"][0]; }
        catch { getManager = false; }
      }

      while (getManager)
      {
        using (DirectorySearcher ds = new DirectorySearcher(de))
        {
          ds.PropertiesToLoad.Add("sAMAccountName");
          ds.PropertiesToLoad.Add("userAccountControl");
          ds.PropertiesToLoad.Add("manager");
          ds.Filter = "(&(objectCategory=user)(distinguishedName=" + managerDName + "))";

          SearchResult sr = ds.FindOne();
          manager = (string)sr.Properties["sAMAccountName"][0];
          uac = (int)sr.Properties["userAccountControl"][0];
          try { managerDName = (string)sr.Properties["manager"][0]; }
          catch { getManager = false; }
        }

        if (uac == 512 || uac == 66048) getManager = false;
      }

      // Get direct reports of current user
      using (DirectorySearcher ds = new DirectorySearcher(de))
      {
        ds.SearchScope = SearchScope.Subtree;
        ds.PropertiesToLoad.Clear();
        ds.PropertiesToLoad.Add("sAMAccountName");
        ds.PropertiesToLoad.Add("userAccountControl");
        ds.ServerPageTimeLimit = TimeSpan.FromSeconds(2);
        ds.Filter = string.Format("(&(objectCategory=user)(manager={0}))", userDName);

        using (SearchResultCollection src = ds.FindAll())
        {
          //Collection<string> tmp = null;
          foreach (SearchResult sr in src)
          {
            // Check if account id disabled
            if ((int)sr.Properties["userAccountControl"][0] == 512 || (int)sr.Properties["userAccountControl"][0] == 66048)
              result.Add((string)sr.Properties["sAMAccountName"][0]);
            // Uncomment to get direct reports of direct reports.
            /*tmp = RetrieveOrganizationStructure((string)sr.Properties["sAMAccountName"][0], ldapBase);
            foreach (string s in tmp)
            {
              result.Add(s);
            }*/
          }
        }
      }
    }
    directReports = result;
  }

  public void EmailUser(string to, string body)
  {
    smtpClient = new SmtpClient("smtp.clearwatercounty.ca", 25);
    smtpClient.UseDefaultCredentials = true;
    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
    smtpClient.EnableSsl = true;

    mailMessage = new MailMessage();
    mailMessage.From = new MailAddress("NoReply@clearwatercounty.ca");
    mailMessage.To.Add(new MailAddress(to + "@clearwatercounty.ca"));
    mailMessage.Subject = "Out of Office";
    mailMessage.IsBodyHtml = true;
    mailMessage.Body = body;

    smtpClient.SendCompleted += new SendCompletedEventHandler(SendAsyncComplete);
    smtpClient.SendAsync(mailMessage, null);
  }

  private void SendAsyncComplete(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
  {
    mailMessage.Dispose();
    smtpClient.Dispose();
  }

  public string WebFullUrl
  {
    get { return myWebFullUrl; }
    set { myWebFullUrl = value; }
  }

  public Dictionary<object, SharePointListItem> MyItems
  {
    get { return myItems; }
  }

  public Dictionary<object, SharePointListItem> ActionItems
  {
    get { return actionItems; }
  }

  public Dictionary<object, SharePointListItem> PendingItems
  {
    get { return pendingItems; }
  }

  public Collection<string> DirectReports
  {
    get { return directReports; }
  }

  public string SAMAccountName
  {
    get { return mySAMAccountName; }
  }
  public string Manager
  {
    get { return manager; }
    set { manager = value; }
  }
  public string Title
  {
    get { return title; }
    set { title = value; }
  }
  public int Id
  {
    get { return id; }
    set { id = value; }
  }
}